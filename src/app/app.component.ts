import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {TokenStorageService} from "./services/token-storage.service";
import {Router} from "@angular/router";
import {UserService} from "./services/user/user.service";
import {LoadService} from "./services/load.service";
import {ClipService} from "./services/clip/clip.service";
import {RoleService} from "./services/role.service";
import {EventService} from "./services/event/event.service";
import {SubscriptionService} from "./services/subscription.service";
import {Subscription} from "rxjs";
import {DeviceService} from "./services/device.service";
import {PrimeNGConfig} from "primeng/api";
import {ClipMethodService} from "./services/clip/clip-method.service";
import {FunctionService} from "./services/function.service";
import {EState} from "./models/enum/EState";


/**
 * Главный компонент отвечает за старт приложения
 * а так же за обновления главных данных
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{

  /**
   * Флаг - показывать или нет контент
   */
  isDisplay: boolean = false;


  /**
   * Подписка на успешное создание чего-либо
   */
  subsCreateSuccess: Subscription;

  constructor(private tokenStorage: TokenStorageService,
              public deviceService:DeviceService,
              private userService: UserService,
              private clipService: ClipService,
              private clipMethodService: ClipMethodService,
              private router: Router,
              public loadService: LoadService,
              public eventService: EventService,
              public roleService: RoleService,
              private config: PrimeNGConfig,
              private functionService: FunctionService,
              private readonly subscriptionService: SubscriptionService) {

    this.loadService.loading();
    this.functionService.setTitle("");

    let user = this.tokenStorage.getUser();

    //Загружаем все типы закрепов
    this.clipService.getClipTypes().subscribe((result) => {
      this.clipService.clipTypes = this.clipMethodService.settingClipType(result);
      // Если пользователь авторизован
      if (user != null) {
        this.userService.setUserCurrent(user);
      }
      this.loadModeratorData();
    })

    //Обновляем данные пользователя - без ожидания
    if (user != null) {
      this.userService.loadCurrentUser().subscribe(result => {
        this.userService.currentUserSetResult(result);

        result.userBox = null;
        this.tokenStorage.saveUser(result);
      })
    }

  }

  /**
   * Для локализации на русский язык
   */
  translate() {
    this.config.setTranslation({
      monthNames: ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],
      dayNames: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
      dayNamesShort: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
      dayNamesMin: ["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
      emptyMessage: 'Нет результатов',
      emptyFilterMessage: 'Нет результатов'
      //translations
    });
  }


  /**
   * Метод обновляет данные связанные
   * с деятельностью модератора
   */
  loadModeratorData(): void {

    this.loadService.loading();
    this.eventService.getAllEventLimits().toPromise().then(result => {
      this.eventService.allEventLimits = result;
    }).then(()=> {
      this.eventService.getAllEventSeasons().subscribe(result => {
        this.eventService.allEventSeasons = result;
        this.eventService.allEventSeasons.sort((x,y) => x.state - y.state)
        this.view();
      })
    })
  }


  /**
   * Метод вызывается когда все готово для отоброжение
   * главного контента
   */
  view(): void {
    this.loadService.isLoad();
    this.isDisplay = true;
  }

  ngOnDestroy(): void {
    this.subsCreateSuccess.unsubscribe();
  }

  ngOnInit(): void {
    // Вызывается при успешном добавлении чего-либо
    this.subsCreateSuccess = this.subscriptionService.createSuccess$.subscribe((data) => {
        this.loadModeratorData();
    });

    this.translate()
  }
}
