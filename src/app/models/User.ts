import {ERole} from "./enum/ERole";
import {UserBox} from "./UserBox";

export interface User {

  id: number;
  name: string;
  email: string;

  protectedId: number;
  role: number;
  state: number;

  userBox?: UserBox | null;
}
