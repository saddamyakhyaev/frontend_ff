
export interface EventLimit{

  id: number;
  name: string;
  timeFrom: string;
  timeTo: string;

}
