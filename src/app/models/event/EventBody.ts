import {EventTime} from "./EventTime";
import {Event} from "./Event";
import {Clip} from "../clip/Clip";

/**
 * Интерфейс для передачи компоненту {@link EventBodyComponent}
 * Хранит само событие и точное время события
 */
export interface EventBody {

  idEventTime: number;
  idDates: number;
  clip: Clip;
  event: Event;
  time: EventTime;
  day: string | Date;
}
