/**
 * Данный класс необходим для работы
 * со временем события
 */
export interface EventTime {

  id: number;
  dates?: string[] | Date[];
  timeFrom: string;
  timeTo: string;

  // /**
  //  * Клиент
  //  */
  // datesFormat?: Date[];
}
