
export interface EventSeason {

  id: number;
  name: string;
  description: string;
  dateFrom: string;
  dateTo: string;

  state: number;
}
