import {EventTime} from "./EventTime";
import {Clip} from "../clip/Clip";
import {EventSeason} from "./EventSeason";

export interface Event {

  id: number;
  description: string;
  dates: EventTime[];
  clips: Clip[];
  createdDate: Date;

  season?: EventSeason;
  createUserId?: number;
  mark?: number;

}
