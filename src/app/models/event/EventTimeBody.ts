/**
 * Данный интерфейс нужен для удобной
 * работы с {@link EventTime}
 */
export interface EventTimeBody {

  date: Date;
  timeFrom: string;
  timeTo: string;

}
