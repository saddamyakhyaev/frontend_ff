import {Clip} from "./clip/Clip";
import {User} from "./User";

export interface Find {

  clips: Clip[];
  users: User[];
}
