import {Mark} from "../mark/Mark";

export interface News{
  id: number;
  name: string;
  description: string;
  nameUnique: string;

  dateTo: Date;
  dateFrom: Date;
  createdDate: Date;

  isPinned: boolean;
  isActive: boolean;
  isNotification: boolean;

  marks?: Mark[];
}
