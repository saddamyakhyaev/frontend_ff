import {Clip} from "../clip/Clip";


export interface Donat {

  id: number;
  coins: number;
  clip: Clip;
}
