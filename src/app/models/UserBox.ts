import {Clip} from "./clip/Clip";

/**
 * Контейнер личных данных пользователя
 */
export interface UserBox {

  favouritesClips: Clip[];
  mainClip: Clip;
  lastNews: number;
}
