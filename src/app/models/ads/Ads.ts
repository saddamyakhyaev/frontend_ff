export interface Ads{
  id: number;
  name: string;
  description: string;
  isActive: boolean;
  createdDate: Date;

  imageUrlPc: string;
  imageUrlMobil: string;

  link?: string;
}
