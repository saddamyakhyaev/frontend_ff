
export interface ClipType {

  id?: number;
  name: string;
  icon: string;
}
