import {Event} from "../event/Event";

export interface Clip {

  id: number;
  name: string;
  description: string;
  typeClipId: number
  state: number;

  events?: Event[];

}
