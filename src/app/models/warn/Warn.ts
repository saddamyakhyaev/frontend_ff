
export interface Warn {
  id?: number;
  text: string;
  codeWarn: number;
  objectId: number;
  objectType?: number;
  objectLink?: number;
  userId?: number;
  createdDate: Date;
  createUserId: number;
}
