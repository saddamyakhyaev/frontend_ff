
export interface Mark {
  id?: number;
  name: string;
  markId?: number;
}
