import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {IndexComponent} from "./components/page/index/index.component";
import {MasterComponent} from "./components/page/master/master.component";
import {LoginComponent} from "./components/page/login/login.component";
import {RegisterComponent} from "./components/page/register/register.component";
import {ClipBodyComponent} from "./components/clip/clip-body/clip-body.component";
import {AdsComponent} from "./components/ads/ads/ads.component";
import {WarnComponent} from "./components/page/warn/warn.component";
import {NewsViewComponent} from "./components/news/news-view/news-view.component";
import {NewsPageComponent} from "./components/page/news/news-page.component";


const routes: Routes = [
  {path: "on", component: MasterComponent},
  {path: "news/:news", component: NewsPageComponent},
  {path: "login", component: LoginComponent},
  {path: "register", component: RegisterComponent},
  {path: "clip/:args", component: ClipBodyComponent},

  {path: "warn", component: WarnComponent},
  {path: "warn/user/:user", component: WarnComponent},
  {path: "warn/object/:object", component: WarnComponent},

  {path: "**", component: IndexComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
