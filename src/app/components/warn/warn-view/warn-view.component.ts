import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NewsComponent} from "../../news/news/news.component";
import {WarnCreateComponent} from "../warn-create/warn-create.component";
import {Subscription} from "rxjs";
import {SubscriptionService} from "../../../services/subscription.service";
import {WarnService} from "../../../services/warn/warn.service";
import {RoleService} from "../../../services/role.service";


@Component({
  selector: 'app-warn-view',
  templateUrl: './warn-view.component.html',
  styleUrls: ['./warn-view.component.css']
})
export class WarnViewComponent implements OnInit, OnDestroy {

  /**
   * Подписка на вызов создания и изменения события
   */
  subsWarn: Subscription;

  /**
   * Компонет для создания предупреждения
   */
  @ViewChild(WarnCreateComponent, {static: false}) warnCreate: WarnCreateComponent;

  constructor(public warnService: WarnService,
              private roleService: RoleService,
              private readonly subscriptionService: SubscriptionService) { }

  ngOnInit(): void {
    this.onSubscription();
    if(this.roleService.isAdmin()) this.load();
  }

  ngOnDestroy(): void {
    this.subsWarn.unsubscribe();
  }

  /**
   * Главная загрузка предупреждений
   */
  load(): void {
    this.warnService.getLastWarns().subscribe(result => {
      this.warnService.lastWarn = result;
    })
  }

  /**
   * Настраиваем прослушки вызовов команд
   */
  onSubscription(): void {
    this.subsWarn = this.subscriptionService.warn$.subscribe((data) => {
      if(data.method) {
        if (data.method === 'create-warn') {
          this.warnCreate.open(data);
        }
      }
    });
  }

}
