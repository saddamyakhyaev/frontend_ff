import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoadService} from "../../../services/load.service";
import {MessageService} from "primeng/api";
import {WarnService} from "../../../services/warn/warn.service";
import {WarnMethodService} from "../../../services/warn/warn-method.service";
import {EWarn} from "../../../models/enum/EWarn";
import {DeviceService} from "../../../services/device.service";

declare var grecaptcha: any;

@Component({
  selector: 'app-warn-create',
  templateUrl: './warn-create.component.html',
  styleUrls: ['./warn-create.component.css']
})
export class WarnCreateComponent implements OnInit, OnDestroy {

  /**
   * Флаг - показывать-ли view-page
   */
  isDisplay: boolean = false;

  /**
   * Объект для работы с формой
   */
  public createWarnForm: FormGroup | null = null;

  /**
   * Код ошидки
   */
  codeWarn: number;

  /**
   * Id объекта связанного с ошибкой
   */
  objectId: number;

  /**
   * Тип объекта связанного с ошибкой (Закреп, Событие)
   */
  objectType: number;

  /**
   * Ссылка связанная с ошибкой
   */
  objectLink: string;

  /**
   * Токен для пакчи
   */
  token: string | undefined;

  patternsWarns: string[] = [];

  constructor(private fb: FormBuilder,
              private messageService: MessageService,
              private warnService: WarnService,
              public deviceService: DeviceService,
              public warnMethodService: WarnMethodService,
              public loadService: LoadService) { }

  ngOnInit(): void {
  }

  /**
   * Метод правил для формы создания новости
   */
  rulesCreateForm(): FormGroup {
    return this.fb.group({
      text: ['', Validators.compose([Validators.required])],
    });
  }


  /**
   * Открываем окно создания
   * @param data
   */
  open(data?: any): void {

    this.isDisplay = true;
    this.createWarnForm = this.rulesCreateForm();
    this.patternsWarns = [];

    this.codeWarn = data.warn.codeWarn;

    if(data.warn.objectId != undefined) this.objectId = data.warn.objectId;
    else this.objectId = -1;

    if(data.warn.objectType != undefined) this.objectType = data.warn.objectType;
    else this.objectType = -1;

    this.objectLink = window.location.href;

    if(this.codeWarn == EWarn.ERROR_ABOUT_CLIP) this.patternsWarns = this.warnMethodService.getPatternsWarnsForClips()
    else if(this.codeWarn == EWarn.ERROR_ABOUT_EVENT) this.patternsWarns = this.warnMethodService.getPatternsWarnsForEvents()

    grecaptcha.reset();
  }

  /**
   * Получаем значение токена
   */
  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
    this.token = captchaResponse;
  }

  /**
   * Закрываем окно
   */
  close(): void {
    this.isDisplay = false;
  }


  /**
   * Событие при клике на кнопку "Отправить"
   */
  submit() {
    if(this.createWarnForm == null) return;

    if (this.createWarnForm.invalid || this.token == undefined) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Введите корректно поля для создания сезона событий'
      });
      return;
    }

    if (this.createWarnForm.get("text")?.value == undefined ||
      this.createWarnForm.get("description")?.value.length < 5 ||
      this.createWarnForm.get("description")?.value.length > 1000) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Текст ошибки от 5 букв'
      });

      return;
    }

    this.loadService.loading();
    // this.dateFrom?.setHours(12,12,12)
    // this.dateTo?.setHours(12,12,12)

    let dataSend: any = {
      text: this.createWarnForm.value.text,
      codeWarn: this.codeWarn,
      objectId: this.objectId,
      objectType: this.objectType,
      objectLink: this.objectLink,
      recaptcha: this.token
    };

    this.warnService.sendWarn(dataSend).subscribe(result => {
      this.messageService.add({severity: 'success', summary: 'Спасибо!', detail: 'Благодаря вам мы становимся лучше'});
      this.loadService.isLoad();
      this.close();
    },error => {
      this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Попробуйте позже'});
      this.loadService.isLoad();
    })
  }

  selectPattern(str: string): void {
    this.patternsWarns = [];
    this.createWarnForm?.get("text")?.setValue(str);
  }

  ngOnDestroy(): void {
    
  }

}
