import {Component, Input, OnInit} from '@angular/core';
import {Warn} from "../../../models/warn/Warn";
import {WarnMethodService} from "../../../services/warn/warn-method.service";

/**
 * Блок Предупреждения
 */
@Component({
  selector: 'app-warn-block',
  templateUrl: './warn-block.component.html',
  styleUrls: ['./warn-block.component.css']
})
export class WarnBlockComponent implements OnInit {

  @Input() warn: Warn;

  constructor(public warnMethodService: WarnMethodService) { }

  ngOnInit(): void {
  }


}
