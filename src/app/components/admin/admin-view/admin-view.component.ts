import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../services/user/user.service";
import {User} from "../../../models/User";
import {UserMethodService} from "../../../services/user/user-method.service";
import {WarnMethodService} from "../../../services/warn/warn-method.service";

@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.css']
})
export class AdminViewComponent implements OnInit {


  /**
   * Список все модераторов
   */
  allUsersIsModerators: User[] = [];

  constructor(private userService: UserService,
              public warnMethodService: WarnMethodService,
              public userMethodService: UserMethodService) { }

  ngOnInit(): void {
    this.open();
  }

  open(): void {
    this.userService.getUsersIsModerator().subscribe(result => {
      this.allUsersIsModerators = result;
    })
  }

}
