import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {Clip} from "../../../models/clip/Clip";
import {User} from "../../../models/User";
import {ClipService} from "../../../services/clip/clip.service";
import {Router} from "@angular/router";
import {ClipMethodService} from "../../../services/clip/clip-method.service";
import {LoadService} from "../../../services/load.service";
import {EClipType} from "../../../models/enum/EClipType";

@Component({
  selector: 'app-clip-link',
  templateUrl: './clip-link.component.html',
  styleUrls: ['./clip-link.component.css']
})
export class ClipLinkComponent implements OnInit {

  /**
   * Если ссылка на закреп
   */
  @Input() clip: Clip;
  /**
   * Если ссылка на пользователя
   */
  @Input() user: User;
  /**
   * Флаг указывающий что события клика обрабатываются родителем
   */
  @Input() flagRouter: string;
  /**
   * Флаг указывающий что бы был крестик закрытия закрепа
   */
  @Input() flagClose: string;

  /**
   * Если нужно перекидывать на новую страницу
   */
  @Input() targer: boolean;

  /**
   * Событие закрытия закрепа (нажатие на крестик),
   * Если есть слушатель, то передаём выбранный закреп родителю
   */
  @Output() close = new EventEmitter<boolean>();

  constructor(public clipService: ClipService,
              public loadService: LoadService,
              public clipMethodService: ClipMethodService,
              private router: Router) {
  }

  ngOnInit(): void {

  }

  /**
   * Клик по закрепу
   */
  clickClip(): void {
    if (!this.flagRouter && this.flagRouter != "flagRouter") {

      this.clipMethodService.clipsCash.push(this.clip)
      if(this.targer){
        const url = this.router.createUrlTree(['/clip', this.clip.id])
        window.open(url.toString(), '_blank')
      } else{
        this.router.navigate(['/clip', this.clip.id]);
      }
    }
  }

  /**
   * Возвращаем ссылку на закреп
   */
  clickClipLink(): string {
    return this.router.createUrlTree(['/clip', this.clip.id]).toString();
  }

  /**
   * Название, например форматируем ФИО для удобного вывода
   */
  public getName(): string {
    if(this.clip.typeClipId == EClipType.TYPE_TEACHER) {
      if(this.clip.name.indexOf(".") == -1){
        let strs: string[] = this.clip.name.split(" ");
        if(strs.length == 3
          && strs[0].length > 2
          && strs[1].length > 2
          && strs[2].length > 2){
          return `${strs[0]} ${strs[1].substr(0,1)}. ${strs[2].substr(0,1)}.`;
        }
      }
    }

    return this.clip.name;
  }


  /**
   * Метод вызывается при нажатии на крестик
   */
  closeClip() {
    this.close.emit(this.clip as any);
  }
}
