import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../../../services/user/user.service";
import {Clip} from "../../../models/clip/Clip";
import {ClipCreateComponent} from "../clip-create/clip-create.component";
import {Subscription} from "rxjs";
import {SubscriptionService} from "../../../services/subscription.service";
import {UserMethodService} from "../../../services/user/user-method.service";
import {ClipService} from "../../../services/clip/clip.service";
import {ClipMethodService} from "../../../services/clip/clip-method.service";
import {RoleService} from "../../../services/role.service";

@Component({
  selector: 'app-clip-view',
  templateUrl: './clip-view.component.html',
  styleUrls: ['./clip-view.component.css']
})
export class ClipViewComponent implements OnInit, OnDestroy {

  @Input() title: string;

  /**
   * Показывать заголовок или нет
   */
  @Input() isHeader: boolean;

  /**
   * Компонет по созданию закрепа
   */
  @ViewChild(ClipCreateComponent, {static: false}) clipCreate: ClipCreateComponent;

  /**
   * Подписка на вызов создания закрепа
   */
  subsClipCreate: Subscription;

  /**
   * Подписка на вызов событий связанных с закрепами
   */
  subsClip: Subscription;

  constructor(public userService: UserService,
              public clipService: ClipService,
              private roleService: RoleService,
              public clipMethodService: ClipMethodService,
              private readonly subscriptionService: SubscriptionService) { }

  ngOnInit(): void {
    this.onSubscription()
    this.load();
  }

  ngOnDestroy(): void {
    this.subsClipCreate.unsubscribe();
    this.subsClip.unsubscribe();
  }



  /**
   * Загрузка данных связанных с закрепом
   */
  load(): void {
    if(this.roleService.isUserAuth()) {
      this.clipMethodService.load();
      this.clipMethodService.loadClipsCreated();
    }
  }

  /**
   * Настраиваем прослушки вызовов команд
   */
  onSubscription(): void {
    // Отвечает за вызов создание закрепа
    this.subsClipCreate = this.subscriptionService.clipCreate$.subscribe((data) => {
      this.clipCreate.open(data);
    });

    this.subsClip = this.subscriptionService.clip$.subscribe((data) => {
      if(data.method) if(data.method === 'update') this.load();
    })
  }


}
