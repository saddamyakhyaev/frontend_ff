import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {switchMap} from "rxjs/operators";
import {LoadService} from "../../../services/load.service";
import {ClipService} from "../../../services/clip/clip.service";
import {Clip} from "../../../models/clip/Clip";
import {ConfirmationService, MessageService} from "primeng/api";
import {ClipCreateComponent} from "../clip-create/clip-create.component";
import {EventCreateComponent} from "../../event/event-create/event-create.component";
import {SubscriptionService} from "../../../services/subscription.service";
import {DeviceService} from "../../../services/device.service";
import {RoleService} from "../../../services/role.service";
import {UserService} from "../../../services/user/user.service";
import {ClipMethodService} from "../../../services/clip/clip-method.service";
import {Subscription} from "rxjs";
import {DonatMethodService} from "../../../services/donat/donat-method.service";
import {ClipRequest} from "../../../models-client/ClipRequest";
import {EWarn} from "../../../models/enum/EWarn";
import {EType} from "../../../models/enum/EType";
import {Event} from "../../../models/event/Event";
import {EventService} from "../../../services/event/event.service";
import {EventMethodService} from "../../../services/event/event-method.service";
import {FunctionService} from "../../../services/function.service";

declare var $: any;

@Component({
  selector: 'app-clip-body',
  templateUrl: './clip-body.component.html',
  styleUrls: ['./clip-body.component.css']
})
export class ClipBodyComponent implements OnInit, OnDestroy {

  /**
   * События закрепа
   */
  events: Event[] = [];

  /**
   * Подписка события связанные с закрепами
   */
  subsClip: Subscription;

  /**
   * Флаг - показывать или нет контент
   */
  isDisplay: boolean = false;

  /**
   * Id закрепа, который нужно отобразить
   */
  id: number;

  /**
   * Тело запроса если оно есть
   */
  clipRequest: ClipRequest | null = null;

  /**
   * Закреп который будет загружен
   * Главный объект компонента
   */
  clip: Clip;

  /**
   * Флаг указывает что мы мы пытаем добавить закреп в избранное
   */
  isFavoriteAdding: boolean = false;

  /**
   * Флаг указывает что мы пытаем удалить закреп из избранных
   */
  isFavoriteDeleting: boolean = false;

  /**
   * Флаг указывает что мы пытаемся добавить главный закреп
   */
  isMainClipAdding: boolean = false;

  /**
   * Флаг показывать кнопки снизу закрепа
   */
  isDisplayBtn: boolean = true;

  /**
   * Подписка на вызов создания события
   */
  subsEvent: Subscription;

  /**
   * Переменная хранит дату последнее добавленное событие
   */
  lastEdit: Date | null = null;


  constructor(public clipService: ClipService,
              public deviceService:DeviceService,
              public roleService:RoleService,
              private route: ActivatedRoute,
              public loadService: LoadService,
              private messageService: MessageService,
              private userService: UserService,
              public donatMethodService: DonatMethodService,
              public clipMethodService: ClipMethodService,
              private confirmationService: ConfirmationService,
              private eventService: EventService,
              public functionService: FunctionService,
              private eventMethodService: EventMethodService,
              private readonly subscriptionService: SubscriptionService,
              private readonly mainMenuService: SubscriptionService) {
    this.loadService.loading(33)
    this.isDisplay = false;
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => params.getAll('args'))
    ).subscribe((data) => {
      this.loadClip(data);
    });

    this.loadEvents();
    this.onSubscription()
  }

  ngOnDestroy(): void {
    this.subsClip.unsubscribe();
  }

  /**
   * Настраиваем прослушки вызовов команд
   */
  onSubscription(): void {
    this.subsClip = this.subscriptionService.clip$.subscribe((data) => {
        if (data.method === 'update'){
          this.loadClip(this.clip.id + "", true);
        }
    });

    // Если событие было создано
    this.subsEvent = this.subscriptionService.event$.subscribe((data) => {
      if(data.method)
        if(data.method == 'create' || data.method == 'delete-event'){
          this.loadEvents();
        }
    });
  }

  /**
   * Загружаем события
   */
  loadEvents(seasonId?: number): void {

    if(this.clip == null || this.clip.id == null) return;
    this.loadService.loading(33)
    this.eventService.getAllEvent(this.clip.id).subscribe(events => {
      this.setupLoadEvents(events);
    })
  }

  /**
   * Обработать загруженные данные
   */
  setupLoadEvents(result: Event[]) {
    this.loadService.isLoad();
    this.events = result;
    this.events.forEach(event => {

      if(this.lastEdit == null || event.createdDate > this.lastEdit) this.lastEdit = event.createdDate;
      event.clips.sort((x,y) => {
        return x.id - y.id;
      })

      console.log("DD: " + event.createdDate)

      // Если в теле запроса есть указания, то мы сразу открываем нужное событие
      if(this.clipRequest != null && this.clipRequest.eventId != null)
        if(this.clipRequest.eventId == event.id){
          let eventBody = this.eventMethodService.getEventBodyByClipRequest(this.clip, event, this.clipRequest);
          // this.clipRequest = this.clipMethodService.getClipRequestOfNull(this.clipRequest);
          if(eventBody != null) this.subscriptionService.EEvent({method: 'open', eventBody: eventBody})
        }
    })
  }


  /**
   * Метод для вызова окна создания события
   */
  openEventCreate(): void {
    this.mainMenuService.openEventCreateTable({clip: this.clip});
  }

  /**
   * Загрузка и установка закрепа
   * @param id закрепа
   */
  loadClip(args: string, flag?: boolean): void {

    this.clipRequest = this.clipMethodService.getClipRequestByArgs(args);
    this.id = this.clipRequest.id;

    if(this.clip && !flag) if(this.clip.id == this.id) {
      this.clipRequest = this.clipMethodService.getClipRequestOfNull(this.clipRequest);
      return;
    }

    this.isDisplay = false;
    this.clipService.getClip(this.id).subscribe(result => {

      this.clip = result;
      this.loadService.isLoad();
      this.isDisplay = true;
      this.loadEvents();

    },error => {
      this.messageService.add({severity:'error', summary:'Ошибка', detail:'Обновите страницу'});
      this.loadService.isLoad();
    })

  }

  /**
   * Меняем в зависимости от момента список избранных
   * @param clipId
   */
  toggleFavouritesClick(clipId: number): void {
    if(this.clipMethodService.isFavorite(clipId)) this.deleteInFavouritesClip(clipId);
    else this.addInFavouritesClip(clipId);
  }

  /**
   * Добавить закреп в избранное
   */
  addInFavouritesClip(clipId: number): void {

    this.isFavoriteAdding = true;
    this.clipService.addInFavouritesClip(clipId).subscribe(result => {
      this.userService.loadCurrentUser().subscribe(result => {
        this.isFavoriteAdding = false;
        this.userService.currentUserSetResult(result);
        this.messageService.add({severity:'success', detail:'Закреп в избранном'});
      });
    })
  }


  /**
   * Вызываем если хотим изменить закреп
   */
  openEditClipPage(): void {
    this.subscriptionService.openClipCreateTable({method: "edit", clip: this.clip});
  }

  /**
   * Открываем окно для создания доната
   */
  openDonatPage(): void {
    this.subscriptionService.EDonat({method: "create", clip: this.clip});
  }

  /**
   * Открываем окно для отправления предупреждения
   */
  openWarnPage(): void {
    this.subscriptionService.EWarn({method: "create-warn", warn: {codeWarn: EWarn.ERROR_ABOUT_CLIP, objectId: this.clip.id, objectType: EType.TYPE_CLIP}});
  }

  /**
   * Удаляем закреп из избранное
   */
  deleteInFavouritesClip(clipId: number): void {

    this.confirmationService.confirm({
      message: 'Удалить закреп из избранного',
      accept: () => {
        this.isFavoriteDeleting = true;
        this.clipService.deleteInFavouritesClip(clipId).subscribe(result => {
          this.clipMethodService.load(()=> {
            this.userService.loadCurrentUser().subscribe(result => {
              this.isFavoriteDeleting = false;
              this.userService.currentUserSetResult(result);
              this.messageService.add({severity: 'success', detail: 'Закреп убран из избранных'});
            });
          });
        })
      }});
  }

  /**
   * Добавляем главный закреп
   */
  addMainClip(clipId: number): void {

    if(this.clipMethodService.isMain(clipId)) {
      this.messageService.add({severity:'warn', detail:'Заменить стартовую страницу можно выбрав другую'});
      return;
    }

    this.isMainClipAdding = true;
    this.clipService.addMainClip(clipId).subscribe(result => {
      this.clipMethodService.load(()=> {
        this.isMainClipAdding = false;
        this.userService.loadCurrentUser().subscribe(result => {
          this.userService.currentUserSetResult(result);
          this.messageService.add({severity: 'success', detail: 'Стартовая страница установленна'});
        });
      });
    })
  }

  /**
   * Когда страница загружена, нужно внести некоторые
   * css значения для адаптации
   */
  initCssParameters(): void {
    //alert($('#clip_header').height())

    $('#app-clip-body').css('padding-top', ($('#clip_header').height() + 40) + 'px');
  }

  /**
   * Открыть/Закрыть бар с кнопками
   */
  toggleButtons(): void {
    if(this.isDisplayBtn) {
      this.isDisplayBtn = false;
    }else {
      this.isDisplayBtn = true;
    }
    setTimeout(()=>{
      this.initCssParameters();
    },1000)
  }

}
