import {Component, Input, OnInit} from '@angular/core';
import {Clip} from "../../../models/clip/Clip";

/**
 * Компонет выводит список закрепов
 */
@Component({
  selector: 'app-clip-list',
  templateUrl: './clip-list.component.html',
  styleUrls: ['./clip-list.component.css']
})
export class ClipListComponent implements OnInit {

  @Input() title: string;
  @Input() clips: Clip[];

  /**
   * Количество элементов для вывода на экран
   */
  countViewItem: number = 4;

  /**
   * Для передачи в {@link ClipLinkComponent}
   */
  @Input() targer: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
