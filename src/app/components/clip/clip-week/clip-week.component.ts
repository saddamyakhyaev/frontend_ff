import {Component, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {Clip} from "../../../models/clip/Clip";
import {EventService} from "../../../services/event/event.service";
import {Event} from "../../../models/event/Event";
import {EventTime} from "../../../models/event/EventTime";
import {CalendarService} from "../../../services/calendar.service";
import {EventBody} from "../../../models/event/EventBody";
import {DeviceService} from "../../../services/device.service";
import {Subscription} from "rxjs";
import {SubscriptionService} from "../../../services/subscription.service";
import {LoadService} from "../../../services/load.service";
import {DOCUMENT} from "@angular/common";
import {MessageService} from "primeng/api";
import {ClipRequest} from "../../../models-client/ClipRequest";
import {EventMethodService} from "../../../services/event/event-method.service";
import {ClipMethodService} from "../../../services/clip/clip-method.service";

declare var $: any;

/**
 * Компонент который выводит события
 * в нужном порядке, в нужном в виде
 */
@Component({
  selector: 'app-clip-week',
  templateUrl: './clip-week.component.html',
  styleUrls: ['./clip-week.component.css']
})
export class ClipWeekComponent implements OnInit {

  /**
   * События закрепа
   */
  @Input() events: Event[] = [];

  /**
   * Дата начала списка недель
   */
  dateStart: Date = new Date();

  /**
   * Количество дней для загрузки
   */
  dayCount: number = 30;

  /**
   * День который обрабатывается в данный момент
   * Используется в шаблоне
   */
  currentDay: Date;

  /**
   * События для дня - {@link currentDay}
   */
  currentDayEvents: EventBody[];

  /**
   * Количество столбцов событий
   */
  col: number = 3;

  /**
   * Закреп для которого будут выводиться события
   */
  @Input() clip: Clip;

  /**
   * Закреп для которого будут выводиться события
   */
  @Input() clipRequest: ClipRequest;


  constructor(@Inject(DOCUMENT) private document: Document,
              private eventService: EventService,
              private eventMethodService: EventMethodService,
              private clipMethodService: ClipMethodService,
              public calendarService: CalendarService,
              private deviceService: DeviceService,
              private messageService: MessageService,
              public loadService: LoadService,
              private readonly subscriptionService: SubscriptionService
  ) {

  }

  ngOnInit(): void {
    this.deviceSetting();
    this.scrollTop();
  }


  /**
   * Настраиваем возможность автоматического
   * увеличения списка недель
   */
  scrollTop(): void {

    let thiss = this;
    $(window).scroll(function(){

      if($('#scroll_flag').length == 0) return;
      var wt = $(window).scrollTop();
      var wh = $(window).height();
      var et = $('#scroll_flag').offset().top;
      var eh = $('#scroll_flag').outerHeight();
      var dh = $(document).height();
      if (wt + wh >= et || wh + wt == dh || eh + et < wh){
        thiss.dayCount += 30;
      }
    });
  }


  /**
   * Метод для настрйоки под девайс
   */
  deviceSetting(): void {
    if(this.deviceService.getWidth() < 500){
      this.col = 12;
    }else if(this.deviceService.getWidth() < 1000){
      this.col = 6;
    }else if(this.deviceService.getWidth() < 1500){
      this.col = 4;
    }else {
      this.col = 3;
    }
  }


  /**
   * Возвраащет информацию о занятости дней на
   * соответсвующей неделе
   * @param date день недели
   */
  getWeekInformation(date: Date): any[] {
    let weeks: any[] = [];

    let iterDate: Date = new Date();
    iterDate.setTime(date.getTime());
    iterDate.setHours(0, 0, 0, 0);

    console.log(date)
    console.log(iterDate)

    for(let i = 1; i < 8; i++) {

      if(this.getEvents(iterDate).length > 0) weeks.push({index: iterDate.getDay(), busy: true, day: this.calendarService.getDayFormat(iterDate), dateWeek: iterDate.getDate()})
      else weeks.push({index: iterDate.getDay(), busy: false, day: this.calendarService.getDayFormat(iterDate), dateWeek: iterDate.getDate()})

      if(iterDate.getDay() == 0) break;
      else iterDate.setDate(iterDate.getDate() + 1)
    }

    console.log(weeks)
    return weeks;
  }


  /**
   * Скроллим к нужному дню
   * @param day
   */
  scroll(day: string): void {

    console.log({scrollTop:$('#week_one_day_' + day).offset().top + "px"})

    let scrollAdd: number = this.deviceService.isPC()?200: 150;
    $('html, body').animate({scrollTop:($('#week_one_day_' + day).offset().top - scrollAdd) + "px"}, 500, 'swing', function() {
      $('#week_one_day_' + day).addClass("scroll_up")
      setTimeout(() => {
        $('#week_one_day_' + day).removeClass("scroll_up");
      },600)
    });
  }

  /**
   * Данный метод уведомляет о том что
   * нет событий на указанный день
   */
  notEventToDay(date: any): void {

    let getDate: Date = new Date();
    getDate.setDate(date);
    console.log(getDate)
    this.messageService.add({
      severity: 'warn',
      summary: 'Внимание',
      detail: getDate.getDate() + " " + this.calendarService.monthYa[getDate.getMonth()] + " нет событий"
    });
  }




  /**
   * Список для цикла в шаблоне
   */
  getArray():
    number[] {
    let arr: number[] = [];
    for (let i = 0; i < this.dayCount; i++) arr.push(i);

    return arr;
  }

  /**
   * День по индексу
   * @param index индекс
   */
  getDayInitilator(index: number): void {
    this.currentDay = new Date();
    this.currentDay.setDate(this.dateStart.getDate() + index);
    this.currentDay.setHours(0, 0, 0, 0);

    this.currentDayEvents = this.getEvents(this.currentDay);
  }

  /**
   * Возвращает события на данный индекс дня
   * @param item
   */
  getEvents(date: Date): EventBody[] {

    let eventBody: EventBody[] = [];

    this.events.forEach(event => {
      event.dates.forEach(time => {
        if(time.dates)
         for(let idDates = 0; idDates < time.dates.length; idDates++){
            let day = time.dates[idDates];
            let sDay = new Date(day);
            sDay.setHours(0, 0, 0, 0);
            // console.log(sDay)
            // console.log(date)
            // console.log("---------------------------------")
            // debugger;
            if (date.valueOf() == sDay.valueOf()) {
              eventBody.push({clip: this.clip, event: event, time: time, day: day, idEventTime: time.id, idDates: idDates})
            }
          }
      })
    })

    eventBody.sort((x,y) => {

      let timeX = new Date("2011-04-11T" + x.time.timeTo + "Z")
      let timeY = new Date("2011-04-11T" + y.time.timeTo + "Z")
      return timeX >= timeY? 1: -1;
    })


    return eventBody;
  }

}
