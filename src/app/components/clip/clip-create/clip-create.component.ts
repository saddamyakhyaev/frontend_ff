import {Component, OnInit} from '@angular/core';
import {ClipService} from "../../../services/clip/clip.service";
import {ClipType} from "../../../models/clip/ClipType";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoadService} from "../../../services/load.service";
import {MessageService} from "primeng/api";
import {SubscriptionService} from "../../../services/subscription.service";
import {Clip} from "../../../models/clip/Clip";
import {ClipMethodService} from "../../../services/clip/clip-method.service";
import {MarkService} from "../../../services/mark/mark.service";
import {Mark} from "../../../models/mark/Mark";
import {MarkMethodService} from "../../../services/mark/mark-method.service";

@Component({
  selector: 'app-clip-create',
  templateUrl: './clip-create.component.html',
  styleUrls: ['./clip-create.component.css']
})
export class ClipCreateComponent implements OnInit {

  /**
   * Флаг - показывать или нет контент
   */
  isLoad: boolean = false;

  /**
   * Флаг - показывать окно или нет
   */
  isDisplay: boolean = false;

  /**
   * Марка закрепа
   */
  mark: Mark | null;

  /**
   * Выбранный тип для закрепа
   */
  selectedClipType: ClipType | null;

  /**
   * Объект для работы с формой
   */
  public clipCreateForm: FormGroup;

  /**
   * Закреп если мы не создаем, а меняем закреп
   */
  editClip: Clip | null = null;



  constructor(public loadService: LoadService,
              private messageService: MessageService,
              public clipService: ClipService,
              private clipMethodService: ClipMethodService,
              private fb: FormBuilder,
              public markService: MarkService,
              public markMethodService: MarkMethodService,
              private readonly subscriptionService: SubscriptionService) {

  }

  /**
   * Метод правил для формы создания закрепа
   */
  rulesClipCreateForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([])],
      typeClip: [{}, Validators.compose([Validators.required])],
      mark: [{}, Validators.compose([])]
    });
  }

  ngOnInit(): void {

  }

  /**
   * Этот метод вызывается из другого компонента для
   * создания закрепа
   */
  open(data?:any): void {

    this.selectedClipType = null;
    this.editClip = null;
    this.mark = null;
    this.clipCreateForm = this.rulesClipCreateForm();

    if(data.method != null){
      if(data.method === 'edit') {
        this.editClip = data.clip;

        this.clipCreateForm.get("name")?.setValue(data.clip.name);
        this.clipCreateForm.get("description")?.setValue(data.clip.description);

        this.clipCreateForm.get("typeClip")?.setValue(this.clipMethodService.getClipTypesOfId(data.clip.typeClipId));
        this.selectedClipType = this.clipMethodService.getClipTypesOfId(data.clip.typeClipId);

        if(data.clip.markId != null) {
          this.clipCreateForm.get("mark")?.setValue(this.markMethodService.getMarkById(data.clip.markId));
          this.mark = this.markMethodService.getMarkById(data.clip.markId)
        }
      }
    }

    if(data.value) {
      this.clipCreateForm.get("name")?.setValue(data.value);
    }
    this.isLoad = true;
    this.isDisplay = true;
  }

  /**
   * Событие при клике на кнопку "создать"
   */
  submit() {
    if (this.clipCreateForm.invalid) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Введите корректно поля для создания закрепа'
      });
      return;
    }

    this.loadService.loading()
    this.clipService.createClip({
      id: this.editClip?.id,
      name: this.clipCreateForm.value.name,
      description: this.clipCreateForm.value.description,
      clipTypeId: this.clipCreateForm.value.typeClip.id,
      markId: this.clipCreateForm.value.mark != null? this.clipCreateForm.value.mark.id: null
    }).subscribe(result => {
      console.log(result)
      this.messageService.add({severity: 'success', summary: 'Ура', detail: this.editClip == null? 'Новый закреп создан!': 'Закреп изменен'});
      this.loadService.isLoad()
      this.subscriptionService.EClip({method:'update'});
      this.isLoad = false;
    }, error => {
      this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Попробуйте позже'});
      this.loadService.isLoad()
    })
  }
}
