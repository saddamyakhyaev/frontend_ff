import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {SubscriptionService} from "../../services/subscription.service";
import {EOtherEvent} from "../../models-client/enum/EOtherEvent";
import {ActionsPageComponent} from "../navigation/actions-page/actions-page.component";
import {EnvironmentService} from "../../services/environment.service";

/**
 * Для работы второстепенных задач собранных в одном компоненте
 */
@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.css']
})
export class OtherComponent implements OnInit, OnDestroy {

  /**
   * Подписка на событии связанные с контактами
   */
  otherNews: Subscription;


  /**
   * Компонет для окна контактов
   */
  @ViewChild(ActionsPageComponent, {static: false}) pageContacts: ActionsPageComponent;

  constructor(private readonly subscriptionService: SubscriptionService,
              public environmentService:EnvironmentService) {

  }

  ngOnInit(): void {
    this.onSubscription();
  }

  ngOnDestroy(): void {
    this.otherNews.unsubscribe();
  }

  /**
   * Настраиваем прослушки вызовов команд
   */
  onSubscription(): void {
    this.otherNews = this.subscriptionService.other$.subscribe((data) => {
      if (data.method == EOtherEvent.CONTACTS_OPEN) this.pageContacts.open();
    });
  }
}

