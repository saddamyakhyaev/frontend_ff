import {Component, ContentChild, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {MenuItem} from "primeng/api";
import {EventCreateComponent} from "../../event/event-create/event-create.component";
import {HeaderTemplateDirective} from "../../../directives/header-template.directive";
import {DeviceService} from "../../../services/device.service";
declare var $: any;

/**
 * Главный класс который будет отображать пункты меню
 */
@Component({
  selector: 'app-actions-page',
  templateUrl: './actions-page.component.html',
  styleUrls: ['./actions-page.component.css']
})
export class ActionsPageComponent implements OnInit {

  isDisplay: boolean = false;

  /**
   * Пункты меню
   */
  @Input() items: MenuItem[] = [];

  /**
   * Название меню
   */
  @Input() title: string;

  /**
   * Название меню
   */
  @Input() description: string;

  /**
   * Фон
   */
  @Input() background: string;

  /**
   * Делаем блок шире
   */
  @Input() width: boolean;


  /**
   * Вывод шаблона вверху меню
   */
  headerTemplate: TemplateRef<any>;

  /**
   * Событие закрытия окна
   */
  @Output() closeEvent = new EventEmitter<boolean>();

  constructor(public deviceService: DeviceService) { }

  ngOnInit(): void {
  }

  /**
   * Открыть меню
   */
  open(): void {
    this.isDisplay = true;

    setTimeout(() => {
      $('#app_actions_page').addClass('open');
    },100)
  }

  /**
   * Закрыть меню
   */
  close(): void {
    this.closeEvent.emit();
    this.isDisplay = false;
  }

  // Получение шаблона контентной области виджета
  @ContentChild(HeaderTemplateDirective, { read: TemplateRef })
  set HeaderTemplate(value: TemplateRef<any>) {
    this.headerTemplate = value;
  }

  get HeaderTemplate(): TemplateRef<any> {
    return this.headerTemplate;
  }


}
