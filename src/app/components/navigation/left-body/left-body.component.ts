import { Component, OnInit } from '@angular/core';
import {RoleService} from "../../../services/role.service";
import {LoadService} from "../../../services/load.service";
import {DeviceService} from "../../../services/device.service";

@Component({
  selector: 'app-left-body',
  templateUrl: './left-body.component.html',
  styleUrls: ['./left-body.component.css']
})
export class LeftBodyComponent implements OnInit {

  constructor(public roleService: RoleService,
              public loadService:LoadService,
              public deviceService: DeviceService) { }

  ngOnInit(): void {
  }

}
