import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MenuItem} from "primeng/api";
import {ProfileComponent} from "../../user/profile/profile.component";
import {ClipCreateComponent} from "../../clip/clip-create/clip-create.component";
import {EventSeasonCreateComponent} from "../../event/season/event-season-create/event-season-create.component";
import {EventCreateComponent} from "../../event/event-create/event-create.component";
import {SubscriptionService} from "../../../services/subscription.service";
import {Subscribable, Subscription} from "rxjs";
import {RoleService} from "../../../services/role.service";
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";
import {EventLimitCreateComponent} from "../../event/limit/event-limit-create/event-limit-create.component";
import {UserService} from "../../../services/user/user.service";
import {EWarn} from "../../../models/enum/EWarn";
import {EType} from "../../../models/enum/EType";
import {EOtherEvent} from "../../../models-client/enum/EOtherEvent";

/**
 * Главные пункты меню и фундаментальные задачи входят в состав
 * этого меню
 */
@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {

  items: MenuItem[];



  constructor(private readonly subscriptionService: SubscriptionService,
              public roleService: RoleService,
              private authService: AuthService,
              public userService: UserService,
              private router: Router) {
  }

  /**
   * Настраиваем пункты меню
   */
  ngOnInit() {

    this.items = [];

    if (this.roleService.isModeratorOf_3_LevelAndHigher()) {
      this.items.push({
          label: 'Сезон событий',
          icon: 'pi pi-fw pi-plus',
          command: () => {
            this.subscriptionService.openEventSeasonCreateTable();
          }
        },
        {
          label: 'Лимит событий',
          icon: 'pi pi-fw pi-plus',
          command: () => {
            this.subscriptionService.openEventLimitCreateTable();
          }
        },
        {
          label: 'Группа',
          icon: 'pi pi-fw pi-plus',
          command: () => {
            this.subscriptionService.EMark({method: 'create-open'});
          }
        })
    }

    if (this.roleService.isModeratorOf_2_LevelAndHigher()) {
      this.items.push(
        {
          label: 'Закреп',
          icon: 'pi pi-fw pi-plus',
          command: () => {
            this.subscriptionService.openClipCreateTable({});
          }
        },
        {
          label: 'Событие',
          icon: 'pi pi-fw pi-plus',
          command: () => {
            this.subscriptionService.openEventCreateTable();
          }
        })
    }

    if(this.roleService.isAdmin()){

      this.items.push(
        {
          label: 'Новость',
          icon: 'pi pi-fw pi-plus',
          command: () => {
            this.subscriptionService.ENews({method: 'create-open'});
          }
        })

      this.items.push(
        {
          label: 'Реклама',
          icon: 'pi pi-fw pi-plus',
          command: () => {
            this.subscriptionService.EAds({method: 'create-open'});
          }
        })
    }

   // if (this.items.length != 0) this.items.push({separator: true});

    this.items.push({
      label: 'Контакты',
      icon: 'pi pi-fw pi-phone',
      command: () => {
        this.subscriptionService.EOther({method: EOtherEvent.CONTACTS_OPEN})
      }
    })

    this.items.push({
      label: 'О нас',
      icon: 'pi pi-fw pi-info-circle',
      command: () => {
        this.subscriptionService.ENews({method: 'open', name: 'about'})
      }
    })

    if (this.roleService.isUserAuth()){
      this.items.push({
        label: 'Сообщить об ошибке',
        icon: 'pi pi-fw pi-send',
        command: () => {
          this.subscriptionService.EWarn({method: "create-warn", warn: {codeWarn: EWarn.ERROR_ABOUT_UNIVERSAL}});
        }
      })
    }

    if (!this.roleService.isUserAuth()){
      this.items.push({
        label: 'Вход',
        icon: 'pi pi-fw pi-sign-in',
        command: () => {
          this.router.navigate(["/login"])
        }
      })
    }

  }

  /**
   * Метод для выхода из аккаунта пользвоателя
   */
  logOut(): void {
    this.authService.logOut();
  }

}
