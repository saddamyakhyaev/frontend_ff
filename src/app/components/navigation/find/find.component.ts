import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Clip} from "../../../models/clip/Clip";
import {FindService} from "../../../services/find.service";
import {Find} from "../../../models/Find";
import {ProfileComponent} from "../../user/profile/profile.component";
import {LoadService} from "../../../services/load.service";
import {Router} from "@angular/router";
import {SubscriptionService} from "../../../services/subscription.service";
import {Subscription} from "rxjs";
import {UserService} from "../../../services/user/user.service";
import {ERole} from "../../../models/enum/ERole";
import {RoleService} from "../../../services/role.service";
import {ClipMethodService} from "../../../services/clip/clip-method.service";
import {User} from "../../../models/User";

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.css']
})
export class FindComponent implements OnInit, OnDestroy {

  /**
   * Объект поиска - получаем с сервера
   */
  find: Find;

  /**
   * Строка поиска
   */
  value: string = "";

  /**
   * Ссылка на таймер поиска
   */
  timerLink: any;

  /**
   * Флаг указывающий работу поиска
   */
  isFinding: boolean = false;


  /**
   * Подписка на успешное создание события
   */
  subsEventCreateSuccess: Subscription;


  /**
   * Событие выбора закрепа,
   * Если есть слушатель, то передаём выбранный закреп родителю
   */
  @Output() selectClip = new EventEmitter<boolean>();


  /**
   * Событие выбора пользователя,
   * Если есть слушатель, то передаём выбранного пользователя родителю
   */
  @Output() selectUser = new EventEmitter<boolean>();

  /**
   * Флаг, показывать меню или нет
   */
  @Input() flagMenu: string;

  /**
   * Флаг, показывать результаты поиска пользователей
   */
  @Input() flagUsers: string;

  /**
   * Флаг, показывать результаты поиска пользователей
   */
  @Input() notClip: string;

  /**
   * Текст в инпуте
   */
  @Input() placeholder: string;

  /**
   * Компонент по работе с пользователем
   */
  @ViewChild(ProfileComponent, {static: false}) profile: ProfileComponent;

  constructor(private findService: FindService,
              private router: Router,
              public roleService: RoleService,
              private clipMethodService: ClipMethodService,
              private readonly subscriptionService: SubscriptionService) {
    this.changeNull();
  }

  ngOnDestroy(): void {
    this.subsEventCreateSuccess.unsubscribe();
  }

  ngOnInit(): void {
    this.onSubscription();
  }

  /**
   * Возвращем текст запроса
   */
  getTextSearching(): string {
    return this.value;
  }

  /**
   * Настраиваем прослушки
   */
  onSubscription(): void {
    // Вызывается при успешном добавлении события
    this.subsEventCreateSuccess = this.subscriptionService.clipCreateSuccess$.subscribe((data) => {
      this.change();
    });
  }

  /**
   * Метод для вызова окна создания закрепа
   */
  openClipCreate(): void {
    this.subscriptionService.openClipCreateTable({value: this.value});
  }

  change() {
    clearTimeout(this.timerLink);
    if (this.value.length > 0) {
      if (!this.findOfFind()) {
        this.isFinding = true;
        this.timerLink = setTimeout(() => {

          this.findService.find(this.value).subscribe((data) => {
            this.find = data;
            console.log(data);
            this.isFinding = false;
          }, error => {
            this.changeNull()
          })
        }, 1000);
      }
    } else this.changeNull();
  }


  /**
   * Поиск в истории и тд
   */
  findOfFind(): boolean {

    this.find = {clips: [], users: []};
    // this.clipMethodService.clipsCreating.forEach(clip => {
    //   if (clip.name.toLowerCase().indexOf(this.value.toLowerCase()) != -1) {
    //     if(this.find.clips.length < 10) this.find.clips.push(clip)
    //   }
    // })

    // this.clipMethodService.clipsCash.forEach(clip => {
    //   if (clip.name.toLowerCase().indexOf(this.value.toLowerCase()) != -1) {
    //     if(this.find.clips.length < 10) this.find.clips.push(clip)
    //   }
    // })

    return this.find.clips.length > 0 ? true : false;
  }

  changeNull(): void {
    this.isFinding = false;
    this.find = {users: [], clips: []};
  }

  /**
   * Открываем page F-пользователя или отпарвляем на верх
   */
  openUser(user: any) {
    if(this.selectUser.observers.length == 0) this.profile.openUserPage(user.id);
    else this.selectUser.emit(user)
  }

  /**
   * Открываем станицу F-закрепа
   * @param id F-закрепа
   */
  openClip(clip: any) {
    this.selectClip.emit(clip)
  }

  /**
   * Очистить поисковый запрос
   */
  clear(): void {
    this.value = "";
    this.changeNull()
  }
}
