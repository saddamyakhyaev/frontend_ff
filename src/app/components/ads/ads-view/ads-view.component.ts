import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AdsService} from "../../../services/ads/ads.service";
import {Subscription} from "rxjs";
import {SubscriptionService} from "../../../services/subscription.service";
import {NewsCreateComponent} from "../../news/news-create/news-create.component";
import {AdsCreateComponent} from "../ads-create/ads-create.component";
import {Ads} from "../../../models/ads/Ads";
import {DeviceService} from "../../../services/device.service";
import {NewsComponent} from "../../news/news/news.component";
import {AdsComponent} from "../ads/ads.component";
import {FunctionService} from "../../../services/function.service";

declare var $: any;

@Component({
  selector: 'app-ads-view',
  templateUrl: './ads-view.component.html',
  styleUrls: ['./ads-view.component.css']
})
export class AdsViewComponent implements OnInit, OnDestroy {


  /**
   * Показывать рекламу или нет
   */
  isDisplay: boolean = false;
  /**
   * Подписка события связанные с рекламой
   */
  subsAds: Subscription;

  /**
   * Та реклама которую покажут.
   */
  showingAds: Ads | null = null;

  /**
   * Компонет для создания рекламы
   */
  @ViewChild(AdsCreateComponent, {static: false}) adsCreate: AdsCreateComponent;

  /**
   * Компонет для вывода одной новости
   */
  @ViewChild(AdsComponent, {static: false}) ads: AdsComponent;

  /**
   * Элемент для загруки картинки
   */
  @ViewChild('img') img: HTMLImageElement;

  constructor(public adsService: AdsService,
              public deviceService: DeviceService,
              public functionService: FunctionService,
              private readonly subscriptionService: SubscriptionService) {
  }

  ngOnInit(): void {
    this.onSubscription();
    this.load()
  }

  ngOnDestroy(): void {
    this.subsAds.unsubscribe();
  }

  getInformation(): void {
    this.subscriptionService.ENews({method: 'open', name: 'ads-about'})
  }

  /**
   * Открыть рекламу
   */
  openAds(): void {
    if (this.showingAds) {
      if (this.showingAds?.link){
        let link = this.showingAds?.link;
        window.open(link, '_blank')
        this.showAds();
      } else this.ads.open(this.showingAds)
    }
  }

  /**
   * Загружаем новости
   */
  load(): void {
    this.adsService.getAllActiveAds().subscribe(result => {
      this.adsService.allAds = result;
      this.showAds()
    })
  }

  /**
   * Выбираем какую рекламу показать
   */
  showAds(): void {
    this.showingAds = null;
    this.isDisplay = false;


    if(this.adsService.allAds.length > 1) {
      this.showingAds = this.adsService.allAds[this.functionService.getRandomInRange(0, (this.adsService.allAds.length - 1))];
    }else if(this.adsService.allAds.length == 1) {
      this.showingAds = this.adsService.allAds[0];
    }else return;

    $('#ads_img_load').attr("src", this.deviceService.isPC()?this.showingAds.imageUrlPc: this.showingAds.imageUrlMobil)
  }

  /**
   * Настраиваем прослушки вызовов команд
   */
  onSubscription(): void {
    this.subsAds = this.subscriptionService.ads$.subscribe((data) => {
      if (data.method) {
        if (data.method === 'create-open') {
          this.adsCreate.open();
        } else if (data.method === 'editor') this.load();
      }
    });
  }

  /**
   * Картинка загруженна
   */
  imgLoad(): void {
    this.isDisplay = true;
    setTimeout(() =>{
      $('#adsMainBlock').addClass('show');
    }, 2000)
  }

}
