import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SubscriptionService} from "../../../services/subscription.service";
import {NewsService} from "../../../services/news/news.service";
import {MessageService} from "primeng/api";
import {LoadService} from "../../../services/load.service";
import {AdsService} from "../../../services/ads/ads.service";

@Component({
  selector: 'app-ads-create',
  templateUrl: './ads-create.component.html',
  styleUrls: ['./ads-create.component.css']
})
export class AdsCreateComponent implements OnInit {


  /**
   * Флаг - показывать-ли view-page
   */
  isDisplay: boolean = false;


  /**
   * Объект для работы с формой
   */
  public createNewsForm: FormGroup | null = null;

  /**
   * Дата начала активности новости
   */
  dataFrom: Date | null;
  /**
   * Дата окончания активности новости
   */
  dateTo: Date | null;

  /**
   * Уведомлять пользователей или нет
   */
  isNotification: boolean = false;

  constructor(private readonly subscriptionService: SubscriptionService,
    private fb: FormBuilder,
    private adsService: AdsService,
    private messageService: MessageService,
    public loadService: LoadService) { }


  /**
   * Открываем окно создания
   * @param data
   */
  open(data?: any): void {
    this.isDisplay = true;
    this.createNewsForm = this.rulesCreateForm();
  }

  /**
   * Закрываем окно создания
   */
  close(): void {
    this.isDisplay = false;
    this.createNewsForm = null;

  }

  /**
   * Метод правил для формы создания новости
   */
  rulesCreateForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],
      text: ['', Validators.compose([])],
      imageUrlPc: ['', Validators.compose([Validators.required])],
      imageUrlMobil: ['', Validators.compose([Validators.required])],
      dateFrom: ['', Validators.compose([Validators.required])],
      dateTo: ['', Validators.compose([Validators.required])],
      link: ['', Validators.compose([])]
    });
  }

  ngOnInit(): void {
  }

  /**
   * Событие при клике на кнопку "создать"
   */
  submit() {
    if(this.createNewsForm == null) return;

    if (this.createNewsForm.invalid) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Введите корректно поля для создания сезона событий'
      });
      return;
    }

    this.loadService.loading();


    let dataSend: any = {
      name: this.createNewsForm.value.name,
      description: this.createNewsForm.value.description,
      text: this.createNewsForm.value.text,
      imageUrlPc: this.createNewsForm.value.imageUrlPc,
      imageUrlMobil: this.createNewsForm.value.imageUrlMobil,
      dateFrom: this.createNewsForm.value.dateFrom,
      dateTo: this.createNewsForm.value.dateTo,
      link: this.createNewsForm.value.link
    };

    this.adsService.createAds(dataSend).subscribe(result => {
      this.messageService.add({severity: 'success', summary: 'Ура', detail: 'Реклама создана'});
      this.loadService.isLoad();
      this.close();
      this.subscriptionService.ENews({method: 'editor'})
    },error => {
      this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Попробуйте позже'});
      this.loadService.isLoad();
    })
  }
}
