import {Component, OnInit, ViewChild} from '@angular/core';
import {News} from "../../../models/news/News";
import {NewsBody} from "../../../models/news/NewsBody";
import {ActionsPageComponent} from "../../navigation/actions-page/actions-page.component";
import {RoleService} from "../../../services/role.service";
import {LoadService} from "../../../services/load.service";
import {NewsService} from "../../../services/news/news.service";
import {SubscriptionService} from "../../../services/subscription.service";
import {Ads} from "../../../models/ads/Ads";
import {AdsBody} from "../../../models/ads/AdsBody";
import {AdsService} from "../../../services/ads/ads.service";

@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.css']
})
export class AdsComponent implements OnInit {

  /**
   * Реклама для вывода
   */
  ads: Ads;

  /**
   * Тело рекламы
   */
  adsBody: AdsBody | null = null;

  isPinnedProcess: boolean = false;
  isActiveProcess: boolean = false;

  /**
   * Компонет для вывода рекламы в Page-окне
   */
  @ViewChild(ActionsPageComponent, {static: false}) page: ActionsPageComponent;


  constructor(public roleService: RoleService,
              public loadService: LoadService,
              private adsService: AdsService,
              private readonly subscriptionService: SubscriptionService) { }

  ngOnInit(): void {
  }

  /**
   * Событие открытия рекламы
   * @param news
   */
  open(ads: Ads): void {

    this.loadService.loadingLazy()
    this.ads = ads;

    this.adsService.getAdsBody(ads.id).subscribe(result => {
      this.adsBody = result;
      this.loadService.isLoad();
      this.page.open();

    }, error => {
      this.loadService.isLoad();
    })

  }

  /**
   * Останавливаем или восстанавливаем рекламу
   */
  activeAds(ads: Ads): void {

    this.isActiveProcess = true;
    if(this.ads.isActive != true){
      this.adsService.activeAds(ads.id).subscribe(result => {
        this.ads.isActive = true;
        this.isActiveProcess = false;

        this.subscriptionService.EAds({method: 'editor'})
      })
    }else {
      this.adsService.activeNotAds(ads.id).subscribe(result => {
        this.ads.isActive = false;
        this.isActiveProcess = false;

        this.subscriptionService.EAds({method: 'editor'})
      })
    }

  }

}
