import {Component, OnInit} from '@angular/core';
import {User} from "../../../models/User";
import {UserService} from "../../../services/user/user.service";
import {LoadService} from "../../../services/load.service";
import {MenuItem, MessageService} from "primeng/api";
import {ERole} from "../../../models/enum/ERole";
import {ClipService} from "../../../services/clip/clip.service";
import {Clip} from "../../../models/clip/Clip";
import {RoleService} from "../../../services/role.service";
import {EState} from "../../../models/enum/EState";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  /**
   * Флаг - показывать или нет контент
   */
  isLoad: boolean = false;

  /**
   * Флаг - показывать окно или нет
   */
  isDisplay: boolean = false;

  /**
   * Сам пользователь
   */
  user: User;

  /**
   * Закрепы созданнные пользователем
   */
  clips: Clip[];

  /**
   * Пункты меню взаимодействия с F-пользователем
   */
  items: MenuItem[];
  /**
   * Роли которые можно устоновить F-пользователю
   */
  itemsSetUpRoles: any[] = [];

  /**
   * Истина если пользователя уже пытаются блокировать
   */
  isBlockingUser: boolean = false;

  constructor(public loadService: LoadService,
              private clipService: ClipService,
              private userService: UserService,
              private roleService: RoleService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {

  }

  /**
   * Метод для настройки пунктов меню
   * взаимодействия с пользовательским профилем
   */
  setMenuItem(): void {

    this.items = [];

    /**
     * Если можно добавить роли F-пользователю,
     * то настраиваем меню для этого
     */
    if (this.itemsSetUpRoles.length > 0) {
      let item = {
        label: 'Дать роль',
        icon: 'pi pi-fw pi-sort-amount-up-alt',
        items: [] as any[]
      };

      this.itemsSetUpRoles.forEach(setUpRole => {
        item.items.push({
          label: setUpRole.label,
          command: () => {
            this.setUpRoleUser(setUpRole.link);
          }
        })
      })

      this.items.push(item)
    }

    if (this.roleService.isModeratorOf_3_LevelAndHigher()) {

      // Заблокировать пользователя
      if (this.user.state == EState.STATE_NORMAL)
        this.items.push({
          label: 'Заблокировать',
          icon: 'pi pi-fw pi-ban',
          command: () => {
            if (!this.isBlockingUser) {
              this.isBlockingUser = true;
              this.userService.blockingUser(this.user.id).subscribe(result => {
                this.isBlockingUser = false;
                this.openUserPage(this.user.id)
                this.messageService.add({severity: 'success', summary: 'Ошибка', detail: 'Пользователь заблокирован'});
              }, error => {
                this.isBlockingUser = false;
                this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Пользователь не блокирован'});
              })
            }
          }
        })

      // Разблокировать пользователя
      if (this.user.state == EState.STATE_BLOCK)
        this.items.push({
          label: 'Разблокировать',
          icon: 'pi pi-fw pi-ban',
          command: () => {
            if (!this.isBlockingUser) {
              this.isBlockingUser = true;
              this.userService.unBlockingUser(this.user.id).subscribe(result => {
                this.isBlockingUser = false;
                this.openUserPage(this.user.id)
                this.messageService.add({severity: 'success', summary: 'Ошибка', detail: 'Пользователь разблокирован'});
                }, error => {
                this.isBlockingUser = false;
                this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Пользователь не разблокирован'});
              })
            }
          }
        })
    }
  }

  // @Input() set userId(id: number) {
  //   if(id != null) {
  //     console.log("CLICK")
  //     this.userService.getUser(id).subscribe(data => {
  //       this.display = true;
  //     })
  //   }
  // }

  setUpRoleUser(role: string): void {

    this.loadService.loading()
    this.userService.setRoleUser(this.user.id, role).subscribe((result) => {
      this.openUserPage(this.user.id)
    }, error => {
      this.loadService.isLoad();
      this.messageService.add({
        severity: 'error',
        summary: 'Ошибка',
        detail: 'Роль не присвоенна данному пользователю'
      });
    })

  }

  /**
   * Загрузка искомого F-пользователя
   * @param id пользователя
   */
  openUserPage(id: number): void {

    this.items = []
    this.loadService.loading();
    this.userService.getUser(id).subscribe(data => {
      this.setUser(data);
      this.isLoad = true;
      this.isDisplay = true;
      this.loadService.isLoad();
      this.setMenuItem();

      this.clips = [];
      this.clipService.getClipsCreatingByUser(this.user.id).subscribe(result => {
        this.clips = result;
      })

    })
  }

  /**
   * Настройки данных F-пользователя полученных с сервера
   * @param data данные с сервера
   */
  setUser(data: any): void {

    this.user = data;
    this.itemsSetUpRoles = []

    /**
     * В соответствии с ролью авторизованного пользователя:
     * - готовим для пунктов меню какие роли можно установить F-пользователю
     */

    switch (this.userService.currentUser.role) {
      case ERole.ROLE_ADMIN:
        this.itemsSetUpRoles.push(
          {label: "X3 Модератор", link: "ROLE_MODERATOR_3"},
          {label: "X2 Модератор", link: "ROLE_MODERATOR_2"},
        )
        break;
      case ERole.ROLE_MODERATOR_3:
        this.itemsSetUpRoles.push(
          {label: "X2 Модератор", link: "ROLE_MODERATOR_2"},
        )
        break;
      case ERole.ROLE_USER:
        break;
    }
  }

  /**
   * Возвращаем в шаблон текст роли пользователя
   */
  showRole(): string {

    switch (this.user.role) {
      case ERole.ROLE_ADMIN:
        return "Админ"
        break;
      case ERole.ROLE_MODERATOR_3:
        return "X3 Модератор"
        break;
      case ERole.ROLE_MODERATOR_2:
        return "X2 Модератор"
        break;
    }

    return ""
  }

  getStateUser(): string {

    switch (this.user.state) {
      case EState.STATE_NORMAL:
        return '<span class="state-normal-master">Положительный</span>';
      case EState.STATE_BLOCK:
        return `<span class="state-block-master">Блокирован</span>`;
    }

    return "";
  }

}
