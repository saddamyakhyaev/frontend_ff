import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {SubscriptionService} from "../../../services/subscription.service";
import {News} from "../../../models/news/News";
import {ActionsPageComponent} from "../../navigation/actions-page/actions-page.component";
import {RoleService} from "../../../services/role.service";
import {NewsService} from "../../../services/news/news.service";
import {NewsBody} from "../../../models/news/NewsBody";
import {LoadService} from "../../../services/load.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FunctionService} from "../../../services/function.service";
import {NewsMethodService} from "../../../services/news/news-method.service";


/**
 * Главный компонент для отображения новости
 */
@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  /**
   * Новость для вывода
   */
  news: News;

  /**
   * Тело новости
   */
  newsBody: NewsBody | null = null;

  isPinnedProcess: boolean = false;
  isActiveProcess: boolean = false;

  /**
   * Менять ссылку при закрытии окна или нет
   */
  routerEdit: boolean = false;


  /**
   * Компонет для вывода новости в Page-окне
   */
  @ViewChild(ActionsPageComponent, {static: false}) page: ActionsPageComponent;

  constructor(public roleService: RoleService,
              public loadService: LoadService,
              private newsService: NewsService,
              public newsMethodService: NewsMethodService,
              private route: ActivatedRoute,
              private router: Router,
              public functionService: FunctionService,
              private readonly subscriptionService: SubscriptionService) { }

  ngOnInit(): void {
  }

  /**
   * Событие открытия новости
   * @param news
   */
  open(news: News, routerEdit?: boolean): void {

    this.loadService.loadingLazy()
    this.news = news;
    if(routerEdit) this.routerEdit = routerEdit;

    this.newsService.getNewsBody(news.id).subscribe(result => {
      this.newsBody = result;
      this.loadService.isLoad();
      this.page.open();
    }, error => {
      this.loadService.isLoad();
    })

  }

  /**
   * Закрываем новость
   */
  close(): void {
    if(this.routerEdit) this.router.navigate(['/'])
  }


  /**
   * Закрепляем или открепляем новость
   */
  pinnedNews(news: News): void {

    this.isPinnedProcess = true;
    if(this.news.isPinned == true){
      this.newsService.pinnedNotNews(news.id).subscribe(result => {
        this.news.isPinned = false;
        this.isPinnedProcess = false;

        this.subscriptionService.ENews({method: 'editor'})
      })
    }else {
      this.newsService.pinnedNews(news.id).subscribe(result => {
        this.news.isPinned = true;
        this.isPinnedProcess = false;

        this.subscriptionService.ENews({method: 'editor'})
      })
    }

  }

  /**
   * Останавливаем или восстанавливаем новость
   */
  activeNews(news: News): void {

    this.isActiveProcess = true;
    if(this.news.isActive != true){
      this.newsService.activeNews(news.id).subscribe(result => {
        this.news.isActive = true;
        this.isActiveProcess = false;

        this.subscriptionService.ENews({method: 'editor'})
      })
    }else {
      this.newsService.activeNotNews(news.id).subscribe(result => {
        this.news.isActive = false;
        this.isActiveProcess = false;

        this.subscriptionService.ENews({method: 'editor'})
      })
    }

  }


  /**
   * Изменить новость
   */
  editNews(news: News): void {
    this.subscriptionService.ENews({method: 'edit-news', news: news, newsBody: this.newsBody})
    this.close();
  }

}
