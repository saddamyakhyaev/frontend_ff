import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {SubscriptionService} from "../../../services/subscription.service";
import {EventLimitCreateComponent} from "../../event/limit/event-limit-create/event-limit-create.component";
import {NewsCreateComponent} from "../news-create/news-create.component";
import {NewsService} from "../../../services/news/news.service";
import {News} from "../../../models/news/News";
import {EventSeasonCreateComponent} from "../../event/season/event-season-create/event-season-create.component";
import {NewsComponent} from "../news/news.component";
import {UserService} from "../../../services/user/user.service";
import {ActivatedRoute} from "@angular/router";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-news-view',
  templateUrl: './news-view.component.html',
  styleUrls: ['./news-view.component.css']
})
export class NewsViewComponent implements OnInit, OnDestroy {

  /**
   * Подписка на вызов создания и изменения события
   */
  subsNews: Subscription;


  /**
   * Количество элементов в списке для вывода
   */
  countViewNews: number = 3;

  /**
   * Показывать заголовок или нет
   */
  @Input() isHeader: boolean;

  /**
   * Показывать список новостей
   */
  @Input() isNotListNews: boolean;

  /**
   * Компонет для создания новостей
   */
  @ViewChild(NewsCreateComponent, {static: false}) newsCreate: NewsCreateComponent;

  /**
   * Компонет для вывода одной новости
   */
  @ViewChild(NewsComponent, {static: false}) news: NewsComponent;

  /**
   * Новости для вывода в списке
   */
  newsView: News[] = [];

  /**
   * Id новости в запросе
   */
  queryNews: number = -1;

  constructor(private readonly subscriptionService: SubscriptionService,
              private userService: UserService,
              private route: ActivatedRoute,
              public newsService: NewsService) { }

  ngOnInit(): void {

    this.onSubscription();
    this.load()

  }
  ngOnDestroy(): void {
    this.subsNews.unsubscribe();
  }

  /**
   * Возвращаем нужное количество для вывода новостей
   */
  getArrayListNews(): News[] {

    let newsReturn: News[] = []

    for(let i = 0, j = 0; i < this.newsService.allNews.length && j < this.countViewNews; i++){
      if(this.newsService.allNews[i].nameUnique.length == 0){
        newsReturn.push(this.newsService.allNews[i]);
        j++;
      }
    }


    return newsReturn;
  }

  /**
   * Загружаем новости
   */
  load(): void {
    this.newsService.getAllActiveNews().subscribe(result => {
      this.newsService.allNews = result;

      let lastNotificationNews: News | null = null;

      this.newsService.allNews.forEach(news => {
        if(this.userService.currentUserBox?.lastNews != null){
          if(news.id > this.userService.currentUserBox?.lastNews && news.isNotification){
              if(lastNotificationNews == null || news.id > lastNotificationNews.id)
                lastNotificationNews = news;
          }
        }
      })

      if(lastNotificationNews != null){
        this.openPageOneNews(lastNotificationNews);
        this.newsService.setUserLastNews(lastNotificationNews.id).subscribe(result => {})
      }

      this.newsService.allNews.sort((x, y) => {
        if(y.isPinned) return 1;
        else if(x.isPinned) return -1;
        else return 0
      })

      // Открываем нужную новость если указана в ссылке
      this.route.paramMap.pipe(
        switchMap(params => params.getAll('news'))
      ).subscribe((data) => {
        let newsId: number = +data;
        this.newsService.allNews.forEach(news => {
          if(news.id == newsId) this.openPageOneNews(news, true);
        });
      });

      this.newsView = this.getArrayListNews();
    })
  }

  /**
   * Настраиваем прослушки вызовов команд
   */
  onSubscription(): void {
    this.subsNews = this.subscriptionService.news$.subscribe((data) => {
        if(data.method) {
          if (data.method === 'create-open') {
            this.newsCreate.open(data);
          }else if(data.method === 'editor') {
            this.load();
          }else if(data.method === 'edit-news') {
            this.newsCreate.open(data)
          }

          if(data.method === 'open') this.openNewsByNameUnique(data.name);
        }
    });
  }

  openNewsByNameUnique(name: string): void {
    this.newsService.allNews.forEach(news => {
      if(news.nameUnique === name) {
        this.openPageOneNews(news);
      }
    })
  }

  /**
   * Вывод одной новости
   * @param news новость
   */
  openPageOneNews(news: News, routerEdit?: boolean): void {
    this.news.open(news, routerEdit);
  }


}
