import { Component, OnInit } from '@angular/core';
import {SubscriptionService} from "../../../services/subscription.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoadService} from "../../../services/load.service";
import {MessageService} from "primeng/api";
import {NewsService} from "../../../services/news/news.service";
import {MarkService} from "../../../services/mark/mark.service";
import {MarkMethodService} from "../../../services/mark/mark-method.service";
import {Mark} from "../../../models/mark/Mark";
import {News} from "../../../models/news/News";
import {FunctionService} from "../../../services/function.service";

@Component({
  selector: 'app-news-create',
  templateUrl: './news-create.component.html',
  styleUrls: ['./news-create.component.css']
})
export class NewsCreateComponent implements OnInit {


  /**
   * Флаг - показывать-ли view-page
   */
  isDisplay: boolean = false;


  /**
   * Объект для работы с формой
   */
  public createNewsForm: FormGroup | null = null;

  /**
   * Дата начала активности новости
   */
  dateFrom: Date | null;
  /**
   * Дата окончания активности новости
   */
  dateTo: Date | null;

  /**
   * Уведомлять пользователей или нет
   */
  isNotification: boolean = false;

  /**
   * Марки для новости
   */
  marks: Mark[] = [];

  /**
   * Ссылка на новость которую нужно изменить
   * NULL если мы создаем новую новость
   */
  editNews: News | null;

  constructor(private readonly subscriptionService: SubscriptionService,
              private fb: FormBuilder,
              private newsService: NewsService,
              private messageService: MessageService,
              public markService: MarkService,
              private functionService: FunctionService,
              public markMethodService: MarkMethodService,
              public loadService: LoadService) { }


  /**
   * Открываем окно создания
   * @param data
   */
  open(data?: any): void {
    this.isDisplay = true;
    this.createNewsForm = this.rulesCreateForm();
    this.marks = [];
    this.editNews = null;
    this.createNewsForm.get("name")?.setValue("");
    this.createNewsForm.get("description")?.setValue("");
    this.createNewsForm.get("nameUnique")?.setValue("");
    this.createNewsForm.get("text")?.setValue("");

    if(data.news != undefined){

      let news: News = data.news;
      this.editNews = data.news;

      this.createNewsForm.get("name")?.setValue(news.name);
      this.createNewsForm.get("description")?.setValue(news.description);
      this.createNewsForm.get("nameUnique")?.setValue(news.nameUnique);
      this.createNewsForm.get("text")?.setValue(data.newsBody.text);

      this.dateFrom = new Date(news.dateFrom);
      this.dateTo = new Date(news.dateTo);

      if(news.marks != undefined) news.marks.forEach(mark => {
        this.marks.push(mark)
      })
    }


  }

  /**
   * Закрываем окно создания
   */
  close(): void {
    this.isDisplay = false;
    this.createNewsForm = null;

  }

  /**
   * Метод правил для формы создания новости
   */
  rulesCreateForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      nameUnique: ['', Validators.compose([])],
      description: ['', Validators.compose([Validators.required])],
      text: ['', Validators.compose([Validators.required])],
      dateFrom: ['', Validators.compose([Validators.required])],
      dateTo: ['', Validators.compose([Validators.required])],
      marks: ['', Validators.compose([])]
    });
  }

  ngOnInit(): void {
  }

  changeIsNotification($event: any): void {
    this.isNotification = $event.checked;
  }

  /**
   * Событие при клике на кнопку "создать"
   */
  submit() {
    if(this.createNewsForm == null) return;

    if (this.createNewsForm.invalid) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Введите корректно поля для создания сезона событий'
      });
      return;
    }

    this.loadService.loading();
    // this.dateFrom?.setHours(12,12,12)
    // this.dateTo?.setHours(12,12,12)

    let dataSend: any = {
      id: this.editNews != null? this.editNews.id: null,
      name: this.createNewsForm.value.name,
      nameUnique: this.createNewsForm.value.nameUnique,
      description: this.createNewsForm.value.description,
      text: this.createNewsForm.value.text,
      dateFrom: this.functionService.getDateSetting(this.dateFrom),
      dateTo: this.functionService.getDateSetting(this.dateTo),
      isNotification: this.isNotification,
      marksId: this.marks.map(mark => mark.id)
    };

    this.newsService.createNews(dataSend).subscribe(result => {
      this.messageService.add({severity: 'success', summary: 'Ура', detail: this.editNews == null? 'Новость создана': 'Новость обновлена'});
      this.loadService.isLoad();
      this.close();
      this.subscriptionService.ENews({method: 'editor'})
    },error => {
      this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Попробуйте позже'});
      this.loadService.isLoad();
    })
  }
}
