import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SubscriptionService} from "../../../services/subscription.service";
import {Subscription} from "rxjs";
import {NewsCreateComponent} from "../../news/news-create/news-create.component";
import {MarkCreateComponent} from "../mark-create/mark-create.component";
import {MarkService} from "../../../services/mark/mark.service";

@Component({
  selector: 'app-mark-view',
  templateUrl: './mark-view.component.html',
  styleUrls: ['./mark-view.component.css']
})
export class MarkViewComponent implements OnInit, OnDestroy {

  /**
   * Все события связанные с марками
   */
  subsMark: Subscription;

  /**
   * Компонет для создания новостей
   */
  @ViewChild(MarkCreateComponent, {static: false}) markCreate: MarkCreateComponent;

  constructor(private readonly subscriptionService: SubscriptionService,
              private markService: MarkService) { }

  ngOnInit(): void {
    this.onSubscription();
    this.load();
  }

  ngOnDestroy(): void {
    this.subsMark.unsubscribe();
  }

  /**
   * Загружаем все что надо для работы с марками
   */
  load(): void {
    this.markService.getAllMarks().subscribe(result => {
      this.markService.allMarks = result;
    })
  }

  /**
   * Настраиваем прослушки вызовов команд
   */
  onSubscription(): void {
    this.subsMark = this.subscriptionService.mark$.subscribe((data) => {
      if(data.method) {
        if (data.method === 'create-open') {
          this.markCreate.open(data);
        }
        if (data.method === 'update') this.load()
      }
    });
  }

}
