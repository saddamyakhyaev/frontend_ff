import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SubscriptionService} from "../../../services/subscription.service";
import {LoadService} from "../../../services/load.service";
import {MessageService} from "primeng/api";
import {Mark} from "../../../models/mark/Mark";
import {MarkService} from "../../../services/mark/mark.service";

@Component({
  selector: 'app-mark-create',
  templateUrl: './mark-create.component.html',
  styleUrls: ['./mark-create.component.css']
})
export class MarkCreateComponent implements OnInit {

  /**
   * Флаг - показывать или нет контент
   */
  isLoad: boolean = false;

  /**
   * Флаг - показывать окно или нет
   */
  isDisplay: boolean = false;

  /**
   * Значения инпута названия марки
   */
  nameValue: string = "";

  /**
   * Объект для работы с формой
   */
  public markCreateForm: FormGroup;

  /**
   * Родительская марка
   */
  parentMark: Mark;

  /**
   * Если мы не создаём, а меняем какой-либо сезон,
   * то тут будет объект который мы меняем
   */
  editMark: Mark | null = null;

  constructor(public loadService: LoadService,
              public markService: MarkService,
              private messageService: MessageService,
              private fb: FormBuilder,
              private readonly subscriptionService: SubscriptionService) {
  }

  ngOnInit(): void {
  }

  /**
   * Метод правил для формы создания марки
   */
  rulesEventCreateForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      parentMark: [{}, Validators.compose([])]
    });
  }


  /**
   * Этот метод вызывается из другого компонента для
   * открытия окна создания или изменения марки
   */
  open(data: any): void {

    this.markCreateForm = this.rulesEventCreateForm();
    this.nameValue = "";
    this.editMark = null;

    if (data.mark != undefined) {
      this.nameValue = data.mark.name;
      this.editMark = data.mark;
    }

    this.markCreateForm.get("name")?.setValue(this.nameValue);
    this.isLoad = true;
    this.isDisplay = true;
  }

  /**
   * Событие при клике на кнопку
   */
  submit() {
    if (this.markCreateForm.invalid) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Введите корректно поля для создания сезона событий'
      });
      return;
    }

    this.loadService.loading()

    let dataSend: any = {
      id: null,
      name: this.markCreateForm.value.name,
      parentMarkId: this.markCreateForm.value.parentMark == null? null: this.markCreateForm.value.parentMark.id
    }

    if (this.editMark != null) dataSend.id = this.editMark.id;

    this.markService.createMark(dataSend).subscribe(result => {
      this.messageService.add({severity: 'success', summary: 'Ура', detail: this.editMark == null? 'Новая марка создана': 'Данные обновлены'});
      this.loadService.isLoad()
      this.isLoad = false;
      this.subscriptionService.EMark({method: "update"})
      }, error => {
      this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Попробуйте позже'});
      this.loadService.isLoad()
    });

  }

}
