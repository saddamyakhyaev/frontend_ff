import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoadService} from "../../../services/load.service";
import {Clip} from "../../../models/clip/Clip";
import {ConfirmationService, MessageService} from "primeng/api";
import {SubscriptionService} from "../../../services/subscription.service";
import {EventService} from "../../../services/event/event.service";
import {EventSeason} from "../../../models/event/EventSeason";
import {Subscription} from "rxjs";
import {EventTime} from "../../../models/event/EventTime";
import {EventLimit} from "../../../models/event/EventLimit";
import {Event} from "../../../models/event/Event";
import {DeviceService} from "../../../services/device.service";
import {EventMethodService} from "../../../services/event/event-method.service";

@Component({
  selector: 'app-event-create',
  templateUrl: './event-create.component.html',
  styleUrls: ['./event-create.component.css']
})
export class EventCreateComponent implements OnInit, OnDestroy {

  /**
   * Флаг - показывать или нет контент
   */
  isLoad: boolean = false;

  /**
   * Флаг - показывать окно или нет
   */
  isDisplay: boolean = false;


  /**
   * Объект для работы с формой
   */
  public clipEventForm: FormGroup;

  /**
   * Массив закрепов которые связанные с событием
   */
  clips: Clip[];

  /**
   * Подписка на успешное создание чего-либо
   */
  subsCreateSuccess: Subscription;

  /**
   * Подписка на всё что связанно с событими
   */
  subsEvent: Subscription;

  /**
   * Время для события
   */
  eventTimes: EventTime[] = [];

  /**
   * Если мы не создаём, в меняем какое-либо событие,
   * то тут будет объект который мы меняем
   */
  editEvent: Event | null;

  /**
   * Описание события
   */
  description: string;

  descriptionSimilarEvents: Event[] = [];

  /**
   * Сезон события
   */
  eventSeason: EventSeason | null;

  constructor(public loadService: LoadService,
              private fb: FormBuilder,
              private messageService: MessageService,
              public eventService: EventService,
              public eventMethodService: EventMethodService,
              public deviceService: DeviceService,
              private confirmationService: ConfirmationService,
              private readonly subscriptionService: SubscriptionService) {
  }

  ngOnInit(): void {
    this.clips = []
    this.onSubscription();
  }

  ngOnDestroy(): void {
    this.subsCreateSuccess.unsubscribe();
    this.subsEvent.unsubscribe();
  }

  /**
   * Настраиваем прослушки
   */
  onSubscription(): void {
    // Вызывается при успешном добавлении чего-то
    this.subsCreateSuccess = this.subscriptionService.createSuccess$.subscribe((data) => {
      this.loadService.loading();
    });

    this.subsEvent = this.subscriptionService.event$.subscribe((data) => {
      if(data.method)
        if(data.method == 'edit-event') this.open(data)
    })
  }

  /**
   * Метод для вызова окна создания сезона событий
   */
  openEventSeasonCreate(): void {
    this.subscriptionService.openEventSeasonCreateTable();
  }

  /**
   * Событие изменения описания
   */
  descriptionInput(): void {
    this.descriptionSimilarEvents = [];
    if(this.description.length < 3) return;

    this.clips.forEach(clip => {
      clip.events?.forEach(ev => {

        if(this.descriptionSimilarEvents.length > 3) return;
        if(ev.description.toLowerCase().indexOf(this.description.toLowerCase()) != -1){
            let flag: boolean = false;
            this.descriptionSimilarEvents.forEach(eventDesc => {
              if (eventDesc.id == ev.id) flag = true;
            })
            if (!flag) this.descriptionSimilarEvents.push(ev);
        }
      })
    })
  }



  /**
   * Открыть событие на изменение из описания
   */
  descriptionSimilarEventsOpen(event: Event): void {

    this.confirmationService.confirm({
      message: 'Перейти на другое событие ?',
      accept: () => {
        this.subscriptionService.EEvent({method: 'edit-event', event: event})
      }});
  }

  /**
   * Добавляем закреп в список связанных закрепов
   */
  addClip(clip: any): void {
    for (let i = 0; i < this.clips.length; i++) {
      if(this.clips[i] == undefined) continue;
      if (this.clips[i].id == clip.id) {
        this.messageService.add({severity: 'warn', detail: 'Данный закреп уже добавлен'});
        return;
      }
    }

    // Загружаем события прислединенных закрепов для поиска текста
    this.eventService.getAllEvent(clip.id).subscribe(result => {
      clip.events = result;
    });

    this.clips.push(clip);
  }


  /**
   * Удаляем закреп из списка связанных закрепов
   */
  deleteClip(clip: any): void {

    for (var i = 0; i < this.clips.length; i++) {
      if (this.clips[i].id == clip.id) {
        delete this.clips[i];
        break;
      }
    }
  }

  /**
   * Создаём новое время события
   * отталкиваясь от лимита
   * @param limit
   */
  selectedEventLimit(limit: EventLimit): void {
    let eventTime: EventTime = {id: -1, timeFrom: limit.timeFrom, timeTo: limit.timeTo};
    for(let i = 0; i < this.eventTimes.length; i++){
        if(this.eventTimes[i].timeTo === eventTime.timeTo && this.eventTimes[i].timeFrom === eventTime.timeFrom){
          this.messageService.add({
            severity: 'warn',
            summary: 'Внимание',
            detail: 'У вас уже создано окно с таким временем. Добавьте даты в него.'
          });

          return;
        }
    }
    this.eventTimes.push(eventTime);
  }

  /**
   * Создаём правила для формы создания события
   */
  createEventCreateForm(): FormGroup {
    return this.fb.group({
      description: ['', Validators.compose([Validators.required])],
      eventSeason: [{}, Validators.compose([Validators.required])]
    });
  }

  /**
   * Этот метод вызывается из другого компонента для
   * создания события
   */
  open(data?: any): void {

    this.loadService.loading();
    this.isLoad = false;

    this.eventTimes = [];
    this.clips = [];
    this.editEvent = null;
    this.eventSeason = null;
    this.clipEventForm = this.createEventCreateForm();

    if (data) {
      if (data.clip != null) this.addClip(data.clip);
      if (data.event != null) {

        let event: Event = data.event;
        this.editEvent = data.event;

        this.description = event.description;
        this.clipEventForm.get("description")?.setValue(this.description);

        let clipsEdit: Clip[] = event.clips;
        clipsEdit.forEach(cl => this.addClip(cl));

        event.dates.forEach(date => this.eventTimes.push(date))
        if(event.season) this.eventSeason = event.season;
      }
    }
    this.isLoad = true;
    this.isDisplay = true;
    this.loadService.isLoad()

  }

  /**
   * Событие при клике на кнопку "создать"
   */
  submit() {

    if (this.clipEventForm.get("description")?.value == undefined ||
      this.clipEventForm.get("description")?.value.length < 5 ||
      this.clipEventForm.get("description")?.value.length > 500) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Описание события должно иметь не менее 5 букв'
      });

      return;
    }

    if(this.clips.length == 0){
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Вам нужно добавить хоть один закреп'
      });

      return;
    }

    if (this.clipEventForm.get("eventSeason")?.value == undefined ||
      this.clipEventForm.get("eventSeason")?.value.name == undefined) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Вам нужно выбрать сезон событий'
      });

      return;
    }

    if(this.eventTimes.length == 0){
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Вам нужно указать время события'
      });
      return;
    }

    for (let i = 0; i < this.eventTimes.length; i++){
      if(this.eventTimes[i].dates == undefined || this.eventTimes[i].dates?.length == 0){
        this.messageService.add({
          severity: 'warn',
          summary: 'Внимание',
          detail: 'Вам нужно указать дни для времени'
        });
        return;
      }
    }

    this.loadService.loading();
    this.eventService.createEvent({
      id: this.editEvent == null? null: this.editEvent.id,
      name: this.clipEventForm.value.name,
      description: this.clipEventForm.value.description,
      clipsId: this.clips.filter(clip => clip != undefined).map(clip => clip.id),
      eventSeasonId: this.clipEventForm.value.eventSeason.id,
      times: this.eventTimes
    }).subscribe(result => {
        this.messageService.add({
          severity: 'success',
          summary: 'Ура',
          detail: 'Событие создано'
        });

        this.isDisplay = false;
        this.isLoad = false;
        this.loadService.isLoad();

        this.subscriptionService.EEvent({method: 'create'});
      },
      error => {
        this.messageService.add({
          severity: 'error',
          summary: 'Ошибка',
          detail: 'Попробуйте позже'
        });
        this.loadService.isLoad();
      })

  }

  /**
   * Удаляем время события из массива
   * @param item
   */
  deleteEvenTime(index: number) {
    delete this.eventTimes[index];
    this.eventTimes = this.eventTimes.filter(x => x != null)
  }
}
