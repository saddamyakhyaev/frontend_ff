import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EventCreateComponent} from "../event-create/event-create.component";
import {Subscription} from "rxjs";
import {SubscriptionService} from "../../../services/subscription.service";
import {EventBody} from "../../../models/event/EventBody";
import {DeviceService} from "../../../services/device.service";
import {EventLimit} from "../../../models/event/EventLimit";
import {EventMethodService} from "../../../services/event/event-method.service";
import {RoleService} from "../../../services/role.service";
import {UserService} from "../../../services/user/user.service";
import {ConfirmationService, MessageService} from "primeng/api";
import {EventService} from "../../../services/event/event.service";
import {LoadService} from "../../../services/load.service";
import {EventTime} from "../../../models/event/EventTime";
import {FunctionService} from "../../../services/function.service";
import {CalendarService} from "../../../services/calendar.service";
import {EventTimeBody} from "../../../models/event/EventTimeBody";
import {ClipMethodService} from "../../../services/clip/clip-method.service";
import {Router} from "@angular/router";
import {EWarn} from "../../../models/enum/EWarn";
import {EType} from "../../../models/enum/EType";

declare var $: any;

/**
 * Данный класс один из главным для
 * вывода информации о событие
 * Открытия окна изменения и обновления данных о событиях
 * и остальных главных манипуляций
 *
 */
@Component({
  selector: 'app-event-view',
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.css']
})
export class EventViewComponent implements OnInit, OnDestroy {


  /**
   * Флаг - показывать ли view-page события
   */
  isDisplayPageView: boolean = false;

  /**
   * Компонет для создания событий
   */
  @ViewChild(EventCreateComponent, {static: false}) eventCreate: EventCreateComponent;

  /**
   * Подписка на вызов создания события
   */
  subsEventCreate: Subscription;

  /**
   * Подписка на вызов всего что связано с событиями
   */
  subsEvent: Subscription;

  /**
   * Хранит тело события для вывода в page-окне
   */
  eventBody: EventBody | null = null;

  /**
   * Лимит который относится к событию
   */
  limit: EventLimit | null;

  constructor(public deviceService: DeviceService,
              public userService: UserService,
              private eventService: EventService,
              private eventMethodService: EventMethodService,
              public clipMethodService: ClipMethodService,
              public roleService: RoleService,
              private confirmationService: ConfirmationService,
              private loadService: LoadService,
              public functionService: FunctionService,
              private messageService: MessageService,
              public calendarService:CalendarService,
              private readonly subscriptionService: SubscriptionService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.onSubscription();
  }

  ngOnDestroy(): void {
    this.subsEventCreate.unsubscribe();
    this.subsEvent.unsubscribe();
  }

  /**
   * Открытие page-окна события
   * @param event
   */
  openPage(eventBody: EventBody): void {
    this.eventBody = eventBody;
    this.limit = this.eventMethodService.getLimitByEventBody(this.eventBody);
    this.isDisplayPageView = true;

    this.router.navigate(['/clip',
      `${this.eventBody.clip.id}_${this.eventBody.event.id}_${this.eventBody.idEventTime}_${this.eventBody.idDates}`]);

  }

  /**
   * Закрываем окно
   */
  hidePage(): void {
    this.isDisplayPageView = false;
    if(this.eventBody != null) this.router.navigate(['/clip', this.eventBody.clip.id]);
  }

  /**
   * Открываем окно для отправления предупреждения
   */
  openWarnPage(): void {
    this.subscriptionService.EWarn({method: "create-warn", warn: {codeWarn: EWarn.ERROR_ABOUT_EVENT, objectId: this.eventBody?.event.id, objectType: EType.TYPE_EVENT}});
  }

  /**
   * Настраиваем прослушки вызовов команд
   */
  onSubscription(): void {
    // Отвечает за вызов создание события
    this.subsEventCreate = this.subscriptionService.eventCreate$.subscribe((data) => {
      this.eventCreate.open(data);
    });

    // Труба для событий событий
    this.subsEvent = this.subscriptionService.event$.subscribe((data) => {
      if (data.method)
        if (data.method === 'open') this.openPage(data.eventBody);
        else if (data.method === 'edit-event') this.closePage();
    })

  }

  /**
   * Открываем page-окно для извенения события
   */
  openEditEventPage(): void {
    this.subscriptionService.EEvent({method: "edit-event", event: this.eventBody?.event});
  }

  /**
   * Открываем page-окно для извенения события
   */
  deleteEventPage(): void {
    this.confirmationService.confirm({
      message: 'Вы точно хотите удалить данное событие?',
      accept: () => {
        this.loadService.loading();
        if (this.eventBody)
          this.eventService.deleteEvent(this.eventBody?.event.id).subscribe(result => {
            this.messageService.add({severity: 'success', detail: 'Удаление прошло успешно'});
            this.loadService.isLoad();
            this.subscriptionService.EEvent({method: 'delete-event'})
          })
        this.closePage();
      }
    });
  }

  /**
   * Закрываем окно события
   */
  closePage(): void {
    this.isDisplayPageView = false;
    this.eventBody = null;
  }


}
