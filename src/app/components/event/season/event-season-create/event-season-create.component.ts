import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ClipService} from "../../../../services/clip/clip.service";
import {EventService} from "../../../../services/event/event.service";
import {LoadService} from "../../../../services/load.service";
import {MessageService} from "primeng/api";
import {SubscriptionService} from "../../../../services/subscription.service";
import {EventLimit} from "../../../../models/event/EventLimit";
import {EventSeason} from "../../../../models/event/EventSeason";
import {RoleService} from "../../../../services/role.service";
import {FunctionService} from "../../../../services/function.service";

@Component({
  selector: 'app-event-season-create',
  templateUrl: './event-season-create.component.html',
  styleUrls: ['./event-season-create.component.css']
})
export class EventSeasonCreateComponent implements OnInit {

  /**
   * Флаг - показывать или нет контент
   */
  isLoad: boolean = false;

  /**
   * Флаг - показывать окно или нет
   */
  isDisplay: boolean = false;

  /**
   * Значения инпута названия сезона событий
   */
  nameValue: string = "";

  /**
   * Значения описания сезона событий
   */
  descriptionValue: string = "";

  /**
   * Дата начала активности сезона
   */
  dataFrom: Date | null;
  /**
   * Дата окончания активности сезона
   */
  dateTo: Date | null;

  /**
   * Если мы не создаём, в меняем какой-либо сезон,
   * то тут будет объект который мы меняем
   */
  editSeason: EventSeason | null;

  /**
   * Объект для работы с формой
   */
  public eventCreateForm: FormGroup;

  constructor(public loadService: LoadService,
              private messageService: MessageService,
              private fb: FormBuilder,
              private eventService: EventService,
              private functionService: FunctionService,
              private readonly subscriptionService: SubscriptionService) { }


  /**
   * Метод правил для формы создания сезона для событий
   */
  rulesEventCreateForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],
      dateFrom: ['', Validators.compose([Validators.required])],
      dateTo: ['', Validators.compose([Validators.required])]
    });
  }

  /**
   * Этот метод вызывается из другого компонента для
   * открытия окна создания сезона для событий
   */
  open(data?: any): void {
    if(data){

      this.dataFrom = null;
      this.dateTo = null;
      this.editSeason = null;

      this.nameValue = "";
      this.descriptionValue = "";

      // Если мы обновляем данные сезоны
      if (data.edit) {
        console.log(data.season)
        this.editSeason = data.season;
        this.nameValue = data.season.name;
        this.dataFrom = new Date(data.season.dateFrom);
        this.dateTo = new Date(data.season.dateTo);
        this.descriptionValue = data.season.description;
      } else {
        if (data.name) this.nameValue = data.name;
      }
    }
    this.eventCreateForm = this.rulesEventCreateForm();
    this.isLoad = true;
    this.isDisplay = true;
  }

  ngOnInit(): void {
  }

  /**
   * Событие при клике на кнопку "создать"
   */
  submit() {
    if (this.eventCreateForm.invalid) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Введите корректно поля для создания сезона событий'
      });
      return;
    }

    this.loadService.loading()

    let dataSend: any = {
      id: null,
      name: this.eventCreateForm.value.name,
      description: this.eventCreateForm.value.description,
      dateFrom: this.functionService.getDateSetting(this.eventCreateForm.value.dateFrom),
      dateTo: this.functionService.getDateSetting(this.eventCreateForm.value.dateTo),
    }

    if (this.editSeason != null) dataSend.id = this.editSeason.id;
    this.eventService.createEventSeason(dataSend).subscribe(result => {

      if (this.editSeason == null) this.messageService.add({severity: 'success', summary: 'Ура', detail: 'Новый сезон создан!'});
      else this.messageService.add({severity: 'success', summary: 'Ура', detail: 'Данные обновлены'});

      this.loadService.isLoad()
      this.isLoad = false;
      this.subscriptionService.openCreateSuccess({season: true});
    }, error => {
      this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Попробуйте позже'});
      this.loadService.isLoad()
    })
  }
}
