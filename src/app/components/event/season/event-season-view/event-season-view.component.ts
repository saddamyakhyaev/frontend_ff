import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EventService} from "../../../../services/event/event.service";
import {EventLimit} from "../../../../models/event/EventLimit";
import {EventSeason} from "../../../../models/event/EventSeason";
import {EventSeasonCreateComponent} from "../event-season-create/event-season-create.component";
import {Subscription} from "rxjs";
import {SubscriptionService} from "../../../../services/subscription.service";
import {ConfirmationService, MessageService} from "primeng/api";
import {LoadService} from "../../../../services/load.service";
import {RoleService} from "../../../../services/role.service";


/**
 * Данный класс один из главным для
 * вывода информации о сезонах событий
 * Открытия окна изменения и обновления данных о сезонах событий
 * и остальных главных манипуляций
 *
 */
@Component({
  selector: 'app-event-season-view',
  templateUrl: './event-season-view.component.html',
  styleUrls: ['./event-season-view.component.css']
})
export class EventSeasonViewComponent implements OnInit, OnDestroy {


  /**
   * Флаг - показывать ли view-page сезона
   */
  isDisplayPageView: boolean = false;

  /**
   * Выбранный сезон
   */
  selectedSeason: EventSeason | null = null;

  /**
   * Показывать список или нет
   */
  @Input() view: boolean;

  /**
   * Компонет для создания и изменения сезона событий
   */
  @ViewChild(EventSeasonCreateComponent, {static: false}) eventSeasonCreate: EventSeasonCreateComponent;

  /**
   * Подписка на вызов создания сезона событий
   */
  subsEventSeasonCreate: Subscription;

  /**
   * Событие изменений и сохранение чего-либо
   */
  subsCreateEvent: Subscription;

  constructor(public eventService: EventService,
              private readonly subscriptionService: SubscriptionService,
              private confirmationService: ConfirmationService,
              private messageService: MessageService,
              public roleService: RoleService,
              public loadService: LoadService) { }

  ngOnInit(): void {
    this.onSubscription();
  }

  ngOnDestroy(): void {
    this.subsEventSeasonCreate.unsubscribe();
  }

  /**
   * Настраиваем прослушки вызовов команд
   */
  onSubscription(): void {
    // Отвечает за вызов создания сезона событий
    this.subsEventSeasonCreate = this.subscriptionService.eventSeasonCreate$.subscribe((data) => {
      this.eventSeasonCreate.open(data);
    });

    this.subsCreateEvent = this.subscriptionService.createSuccess$.subscribe((data) => {
      if(data.season) {
        this.closeViewSeason();
      }
    });
  }

  /**
   * Метод открывает view-page лимита
   * @param item лимит
   */
  openViewSeason(item: EventSeason) {
    this.selectedSeason = item;
    this.isDisplayPageView = true;
  }

  /**
   * Метод закрывает view-page лимита
   * @param item лимит
   */
  closeViewSeason() {
    this.isDisplayPageView = false;
  }

  /**
   * Метод закрывает view-page лимита и обновляет страницу
   */
  closeViewSeasonReload() {
    this.closeViewSeason()
    setTimeout(() => {
      window.location.reload();
    },1500)
  }

  /**
   * Открыть окно изменения лимита
   * @param limit - сам лимит для изменения
   */
  openEditSeason(season: any) {
    this.subscriptionService.openEventSeasonCreateTable({edit: true, season: season})
  }

  /**
   * Удаляем сезон
   */
  deleteEventSeason(eventSeasonId: number) {
    this.confirmationService.confirm({
      message: 'Вы точно хотите удалить данный сезон?',
      accept: () => {

        this.loadService.loading();
        this.closeViewSeason();
        this.eventService.deleteEventSeason(eventSeasonId).subscribe(result => {
          this.messageService.add({severity: 'success', detail: 'Удаление прошло успешно'});
          this.closeViewSeasonReload();
        }, error => {
          this.messageService.add({severity: 'error', detail: 'Попробуйте позже'});
          this.loadService.isLoad();
        })
      }
    });
  }

  /**
   * Активируем сезон событий
   */
  activeEventSeason(eventSeasonId: number) {

    this.loadService.loading();
    this.eventService.activeEventSeason(eventSeasonId).subscribe(result => {
      this.messageService.add({severity: 'success', detail: 'Сезон активен'});
      this.closeViewSeasonReload();
    })
  }

  /**
   * Блокируем сезон событий
   */
  blockEventSeason(eventSeasonId: number) {
    this.loadService.loading();
    this.eventService.blockEventSeason(eventSeasonId).subscribe(result => {
      this.messageService.add({severity: 'success', detail: 'Сезон блокирвован'});
      this.subscriptionService.openCreateSuccess({deleteEventSeason: true});
      this.closeViewSeasonReload();
    })
  }

}
