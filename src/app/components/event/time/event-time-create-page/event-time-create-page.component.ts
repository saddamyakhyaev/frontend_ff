import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {EventService} from "../../../../services/event/event.service";
import {EventTime} from "../../../../models/event/EventTime";
import {EventLimit} from "../../../../models/event/EventLimit";
import {FunctionService} from "../../../../services/function.service";


/**
 * Окно в котором мы для времени события выбираем лимит
 */
@Component({
  selector: 'app-event-time-create-page',
  templateUrl: './event-time-create-page.component.html',
  styleUrls: ['./event-time-create-page.component.css']
})
export class EventTimeCreatePageComponent implements OnInit {

  /**
   * Флаг - показывать окно или нет
   */
  isDisplay: boolean = false;

  /**
   * Лимит который мы ищем, либо выбираем из
   * уже созданных, либо указываем в ручную
   */
  selectedEventLimit: EventLimit;

  /**
   * Событие выбора лимита
   */
  @Output() selected = new EventEmitter<EventLimit>();

  /**
   * Время начала события
   */
  timeFrom: Date;
  /**
   * Конец события
   */
  timeTo: Date;

  constructor(public eventService: EventService,
              private functionService: FunctionService) {
  }

  open(): void {
    this.isDisplay = true;
  }

  ngOnInit(): void {
  }

  selectInputTime(limit?: EventLimit): void {
    if(!limit){
      this.selectedEventLimit = {id: -1, timeTo: "", timeFrom: "", name: ""};
      this.selectedEventLimit.timeFrom = this.functionService.getTimeFormatToSendToServer(this.timeFrom);
      this.selectedEventLimit.timeTo = this.functionService.getTimeFormatToSendToServer(this.timeTo);
      this.selected.emit(this.selectedEventLimit);
    }else this.selected.emit(limit);

    this.isDisplay = false;
  }

}
