import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EventLimit} from "../../../../models/event/EventLimit";
import {EventTime} from "../../../../models/event/EventTime";


/**
 * Данный компонент создаёт время для события
 * Главным образом служит для {@link EventCreateComponent}
 */
@Component({
  selector: 'app-event-time-create',
  templateUrl: './event-time-create.component.html',
  styleUrls: ['./event-time-create.component.css']
})
export class EventTimeCreateComponent implements OnInit {


  /**
   * Лимит для установки границ времени
   */
  @Input() eventTime: EventTime;

  /**
   * Событие - удалить блок
   */
  @Output() delete = new EventEmitter<boolean>();

  // /**
  //  * Даты для события
  //  */
  // dates: Date[];


  constructor() {

  }

  ngOnInit(): void {
    if(this.eventTime && this.eventTime.dates)
      for(let i = 0; i < this.eventTime.dates?.length; i++){
        this.eventTime.dates[i] = new Date(this.eventTime.dates[i]);
      }
  }

  /**
   * Если дату добавили или изменили
   */
  changeDate(): void {
    this.eventTime.dates?.forEach(date => {
        if(date instanceof Date){
          date.setHours(14,0,0);
        }
      })
  }

}
