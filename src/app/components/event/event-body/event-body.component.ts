import {Component, Input, OnInit} from '@angular/core';
import {Event} from "../../../models/event/Event";
import {EventService} from "../../../services/event/event.service";
import {EventTime} from "../../../models/event/EventTime";
import {EventLimit} from "../../../models/event/EventLimit";
import {EventBody} from "../../../models/event/EventBody";
import {MenuItem} from "primeng/api";
import {SubscriptionService} from "../../../services/subscription.service";
import {EventMethodService} from "../../../services/event/event-method.service";
import {ClipService} from "../../../services/clip/clip.service";
import {ClipMethodService} from "../../../services/clip/clip-method.service";
import {Clip} from "../../../models/clip/Clip";

@Component({
  selector: 'app-event-body',
  templateUrl: './event-body.component.html',
  styleUrls: ['./event-body.component.css']
})
export class EventBodyComponent implements OnInit {


  /**
   * В данном обьекте хранится:
   * event - само событие
   * time - время события которое относится именно к этому компоненту
   * day - день соответственно
   */
  @Input() eventBody: EventBody;

  /**
   * Лимит который относится к событию
   */
  limit: EventLimit | null;

  /**
   * Закреп для которого будут выводиться события
   */
  @Input() clip: Clip;


  /**
   * Функции для взаимодействия с событием
   */
  menuItem: MenuItem[];

  constructor(public eventService: EventService,
              public clipMethodService: ClipMethodService,
              private eventMethodService: EventMethodService,
              private readonly subscriptionService: SubscriptionService) {

  }

  ngOnInit(): void {

    this.limit = this.eventMethodService.getLimitByEventBody(this.eventBody);

  }

  /**
   * Откыть page-окно события
   */
  open(): void {
    this.subscriptionService.EEvent({method: 'open', eventBody: this.eventBody})
  }


  getDescription(): string {

    let str = this.eventBody.event.description.length > 300? this.eventBody.event.description.substr(0,299) + "...": this.eventBody.event.description;
    if(this.eventBody.event.description != null)
      return this.eventBody.event.description.replace("\n","<\/br>");
    else return "";
  }


}
