import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EventService} from "../../../../services/event/event.service";
import {EventLimit} from "../../../../models/event/EventLimit";
import {SubscriptionService} from "../../../../services/subscription.service";
import {EventCreateComponent} from "../../event-create/event-create.component";
import {Subscription} from "rxjs";
import {EventLimitCreateComponent} from "../event-limit-create/event-limit-create.component";
import {ConfirmationService, MessageService} from "primeng/api";
import {LoadService} from "../../../../services/load.service";
import {RoleService} from "../../../../services/role.service";


/**
 * Данный класс один из главным для
 * вывода информации о лимите событий
 * Открытия окна изменения и обновления данных о лимите событий
 * и остальных главных манипуляций
 *
 */
@Component({
  selector: 'app-event-limit-view',
  templateUrl: './event-limit-view.component.html',
  styleUrls: ['./event-limit-view.component.css']
})
export class EventLimitViewComponent implements OnInit, OnDestroy {


  /**
   * Флаг - показывать ли view-page лимита
   */
  isDisplayPageView: boolean = false;

  /**
   * Выбранный лимит
   */
  selectedLimit: EventLimit | null = null;

  /**
   * Показывать список или нет
   */
  @Input() view: boolean;

  /**
   * Компонет для создания лимита событий
   */
  @ViewChild(EventLimitCreateComponent, {static: false}) eventLimitCreate: EventLimitCreateComponent;

  /**
   * Подписка на вызов создания и изменения события
   */
  subsEventLimitCreate: Subscription;

  /**
   * Событие изменений и сохранение чего-либо
   */
  subsCreateEvent: Subscription;

  constructor(public eventService: EventService,
              private readonly subscriptionService: SubscriptionService,
              private confirmationService: ConfirmationService,
              private messageService: MessageService,
              public roleService: RoleService,
              private loadService: LoadService){ }

  ngOnInit(): void {
    console.log(this.eventService.allEventLimits)
    this.onSubscription()
  }

  ngOnDestroy(): void {
    this.subsEventLimitCreate.unsubscribe();
    this.subsCreateEvent.unsubscribe();
  }

  /**
   * Настраиваем прослушки вызовов команд
   */
  onSubscription(): void {

    // Отвечает за вызов создание и изменения события
    this.subsEventLimitCreate = this.subscriptionService.eventLimitCreate$.subscribe((data) => {
      this.eventLimitCreate.open(data);
    });

    this.subsCreateEvent = this.subscriptionService.createSuccess$.subscribe((data) => {
      if(data.limit) {
        this.closeViewLimit();
      }
    });

  }

  /**
   * Метод открывает view-page лимита
   * @param item лимит
   */
  openViewLimit(item: EventLimit) {
      this.selectedLimit = item;
      this.isDisplayPageView = true;
  }

  /**
   * Метод закрывает view-page лимита
   */
  closeViewLimit() {
    this.isDisplayPageView = false;
  }

  /**
   * Открыть окно изменения лимита
   * @param limit - сам лимит для изменения
   */
  openEditLimit(limit: any) {
    this.subscriptionService.openEventLimitCreateTable({edit: true, limit: limit})
  }

  /**
   * Удаляем лимит
   */
  deleteEventLimit(eventLimitId: number) {
    this.confirmationService.confirm({
      message: 'Вы точно хотите удалить данный лимит?',
      accept: () => {

        this.loadService.loading();
        this.closeViewLimit();
        this.eventService.deleteEventLimit(eventLimitId).subscribe(result => {
          this.messageService.add({severity: 'success', detail: 'Удаление прошло успешно'});
          this.loadService.isLoad();
          this.subscriptionService.openCreateSuccess({deleteEventLimit: true});
        }, error => {
          this.messageService.add({severity: 'error', detail: 'Попробуйте позже'});
          this.loadService.isLoad();
        })
      }
    });
  }

}
