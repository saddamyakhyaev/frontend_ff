import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoadService} from "../../../../services/load.service";
import {MessageService} from "primeng/api";
import {EventService} from "../../../../services/event/event.service";
import {SubscriptionService} from "../../../../services/subscription.service";
import {EventLimit} from "../../../../models/event/EventLimit";
import {FunctionService} from "../../../../services/function.service";

@Component({
  selector: 'app-event-limit-create',
  templateUrl: './event-limit-create.component.html',
  styleUrls: ['./event-limit-create.component.css']
})
export class EventLimitCreateComponent implements OnInit {

  /**
   * Флаг - показывать окно или нет
   */
  isDisplay: boolean = false;

  /**
   * Значения инпута названия лимита событий
   */
  nameValue: string = "";

  /**
   * Время начала события
   */
  timeFrom: Date | null;
  /**
   * Конец события
   */
  timeTo: Date | null;

  /**
   * Если мы не создаем, в меняем какой-либо лимит,
   * то тут будет объект который мы меняем
   */
  editLimit: EventLimit | null;

  /**
   * Объект для работы с формой
   */
  public eventLimitCreateForm: FormGroup;

  constructor(public loadService: LoadService,
              private messageService: MessageService,
              private fb: FormBuilder,
              private eventService: EventService,
              private functionService: FunctionService,
              private readonly subscriptionService: SubscriptionService) {

    this.eventLimitCreateForm = this.rulesEventLimitCreateForm();
  }


  /**
   * Метод правил для формы создания лимита событий
   */
  rulesEventLimitCreateForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      timeFrom: ['', Validators.compose([Validators.required])],
      timeTo: ['', Validators.compose([Validators.required])]
    });
  }

  /**
   * Этот метод вызывается из другого компонента для
   * открытия окна создания лимита событий
   */
  open(data?: any): void {

    this.editLimit = null;
    this.nameValue = "";
    if (data) {
      // Если мы обновляем данные лимита
      if (data.edit) {
        this.editLimit = data.limit;
        this.nameValue = data.limit.name;
        this.timeFrom = new Date('2011-04-11T' + data.limit.timeFrom);
        this.timeTo = new Date('2011-04-11T' + data.limit.timeTo);
      } else {
        if (data.name) this.nameValue = data.name;
        else this.nameValue = "";

        this.timeFrom = null;
        this.timeTo = null;
      }
    }
    this.isDisplay = true;
  }

  /**
   * Закрыть окно
   */
  close(): void {
    this.isDisplay = false;
  }

  ngOnInit(): void {
  }

  /**
   * Событие при клике на кнопку "создать"
   */
  submit() {
    if (this.eventLimitCreateForm.invalid) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Введите корректно поля для создания лимита событий'
      });
      return;
    }


    this.loadService.loading()

    let dataSend: any = {
      id: null,
      name: this.eventLimitCreateForm.value.name,
      timeFrom: this.functionService.getTimeFormatToSendToServer(new Date(this.eventLimitCreateForm.value.timeFrom)),
      timeTo: this.functionService.getTimeFormatToSendToServer(new Date(this.eventLimitCreateForm.value.timeTo)),
    };

    if (this.editLimit != null) dataSend.id = this.editLimit.id;

    this.eventService.createEventLimit(dataSend).subscribe(result => {

      if (this.editLimit == null) this.messageService.add({
        severity: 'success',
        summary: 'Ура',
        detail: 'Новый лимит создан!'
      });
      else this.messageService.add({severity: 'success', summary: 'Ура', detail: 'Данные обновлены'});
      alert("DDDD")

      this.loadService.isLoad()
      this.isDisplay = false;
      this.subscriptionService.openCreateSuccess({limit: true});
    }, error => {
      this.messageService.add({severity: 'error', summary: 'Ошибка', detail: 'Попробуйте позже'});
      this.loadService.isLoad()
    })
  }
}

