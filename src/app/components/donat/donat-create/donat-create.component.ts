import {Component, OnDestroy, OnInit} from '@angular/core';
import {SubscriptionService} from "../../../services/subscription.service";
import {Subscription} from "rxjs";
import {DeviceService} from "../../../services/device.service";
import {Clip} from "../../../models/clip/Clip";
import {LoadService} from "../../../services/load.service";
import {User} from "../../../models/User";
import {MessageService} from "primeng/api";
import {DonatService} from "../../../services/donat/donat.service";

@Component({
  selector: 'app-donat-create',
  templateUrl: './donat-create.component.html',
  styleUrls: ['./donat-create.component.css']
})
export class DonatCreateComponent implements OnInit {

  /**
   * Флаг - показывать окно или нет
   */
  isDisplay: boolean = false;

  /**
   * Закреп привязанный к донату
   */
  clip: Clip | null = null;

  /**
   * Пользователь привязанный к донату
   */
  user: User | null = null;

  /**
   * Количество вложенной манеты
   */
  coins: string = "";

  constructor(public deviceService: DeviceService,
              private donatService: DonatService,
              private messageService: MessageService,
              public loadService: LoadService) { }

  ngOnInit(): void {
  }

  open(clip?: Clip): void {
    this.clip = null;
    this.user = null;
    this.coins = "";

    if(clip) this.clip = clip;
    this.isDisplay = true;
  }

  /**
   * Добавляем закреп в список связанных закрепов
   */
  addUser(user: any): void {
    this.user = user;
  }

  /**
   * Отправки доната
   */
  submit() {

    if(this.coins.length == 0) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Укажите ценну вклада'
      });
      return;
    }

    if(this.clip == null) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Нужно добавить закреп'
      });
      return;
    }

    if(this.user == null) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Внимание',
        detail: 'Нужно добавить пользователя'
      });
      return;
    }

    this.donatService.createOrAddDonat({
      clipId: this.clip?.id,
      userId: this.user?.id,
      coins: this.coins
    }).subscribe(result => {
      this.messageService.add({
        severity: 'success',
        summary: 'Ура',
        detail: 'Донат учтен'
      });

      this.isDisplay = false;
    },error => {
      this.messageService.add({
        severity: 'error',
        summary: 'Ошибка',
        detail: error
      });
    })

  }


}
