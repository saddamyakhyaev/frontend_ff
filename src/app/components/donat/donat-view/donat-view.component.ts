import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {SubscriptionService} from "../../../services/subscription.service";
import {ClipCreateComponent} from "../../clip/clip-create/clip-create.component";
import {DonatCreateComponent} from "../donat-create/donat-create.component";
import {DonatService} from "../../../services/donat/donat.service";
import {Clip} from "../../../models/clip/Clip";
import {Router} from "@angular/router";

@Component({
  selector: 'app-donat-view',
  templateUrl: './donat-view.component.html',
  styleUrls: ['./donat-view.component.css']
})
export class DonatViewComponent implements OnInit, OnDestroy {

  /**
   * Подписка события связанные с закрепами
   */
  subsDonat: Subscription;

  /**
   * Компонет по созданию доната
   */
  @ViewChild(DonatCreateComponent, {static: false}) donatCreate: DonatCreateComponent;

  /**
   * Общая сумма
   */
  allSum: number;


  constructor(public donatService: DonatService,
              private router: Router,
              private readonly subscriptionService: SubscriptionService) { }

  ngOnInit(): void {
    this.onSubscription()
    this.load()
  }

  /**
   * Загружаем все необходимые данные
   */
  load(): void {
      this.donatService.getAllDonates().subscribe(result => {
        this.donatService.allDonates = result;
        this.allSum = 0;

        this.donatService.allDonates.sort((x,y) => y.coins - x.coins)
        this.donatService.allDonates.forEach(donat => {
          this.allSum += donat.coins;
        })
      })
  }

  /**
   * Переход в закреп
   */
  chartClick(clip: Clip): void {
    const url = this.router.createUrlTree(['/clip', clip.id])
    window.open(url.toString(), '_blank')
  }


  ngOnDestroy(): void {
    this.subsDonat.unsubscribe();
  }

  /**
   * Настраиваем прослушки вызовов команд
   */
  onSubscription(): void {
    this.subsDonat = this.subscriptionService.donat$.subscribe((data) => {
      if(data.method)
        if (data.method === 'create') this.donatCreate.open(data?.clip)
    });
  }

  /**
   * Информация о сути донатах
   */
  getInformation(): void {
    this.subscriptionService.ENews({method: 'open', name: 'donate-about'})
  }

}
