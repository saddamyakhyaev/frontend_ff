import {Component, Input, OnInit} from '@angular/core';

/**
 * Линия в процентном соотношении
 */
@Component({
  selector: 'app-chart-line',
  templateUrl: './chart-line.component.html',
  styleUrls: ['./chart-line.component.css']
})
export class ChartLineComponent implements OnInit {

  /**
   * Название пункта
   */
  @Input() name: string;

  /**
   * Сумма самого пункта
   */
  @Input() ownCount: number;

  /**
   * Общая сумма
   */
  @Input() allCount: number;

  /**
   * Порядковый номер
   */
  @Input() index: number;

  /**
   * Процент пункта от общей суммы
   */
  width: number;

  constructor() {

  }

  ngOnInit(): void {
    this.width = (this.ownCount * 100) / this.allCount;
  }

}
