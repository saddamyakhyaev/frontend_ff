import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {switchMap} from "rxjs/operators";
import {WarnService} from "../../../services/warn/warn.service";
import {Warn} from "../../../models/warn/Warn";
import {LoadService} from "../../../services/load.service";
import {MessageService} from "primeng/api";
import {EWarn} from "../../../models/enum/EWarn";

/**
 * Страница для администрирования предупреждений
 */
@Component({
  selector: 'app-warn',
  templateUrl: './warn.component.html',
  styleUrls: ['./warn.component.css']
})
export class WarnComponent implements OnInit {

  /**
   * Ошибки для вывода
   */
  warns: Warn[] = [];
  title: string;

  /**
   * Код поиска по warn: поп пользователю, по событию и тд
   */
  typeWarn: EWarn;


  constructor(private route: ActivatedRoute,
              private loadService: LoadService,
              private messageService: MessageService,
              private warnService: WarnService) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => params.getAll('user'))
    ).subscribe((data) => {
      this.loadUsersWarn(+data);
    });

    this.loadAllWarn();
  }

  /**
   * Загрузка всех ошибок
   */
  loadAllWarn(): void {
    this.title = "Последние ошибки";
    this.typeWarn = EWarn.ERROR_ABOUT_UNIVERSAL;
    this.warnService.getLastWarns().subscribe(result => {
      this.warns = result;
    });
  }
  /**
   * Загрузка данных по пользователю
   */
  loadUsersWarn(userId: number): void {

    this.title = "Данные по пользователю с ID: " + userId;
    this.typeWarn = EWarn.ERROR_ABOUT_CLIP;
    this.loadService.loading();
    this.warnService.getLastWarnsByUser(userId).subscribe(result => {
      this.warns = result;
      this.loadService.isLoad();
    })
  }


  /**
   * Очищаем данные о предупреждениях
   */
  clear(): void {
    this.loadService.loading();
    if(this.typeWarn == EWarn.ERROR_ABOUT_CLIP) {
      this.route.paramMap.pipe(
        switchMap(params => params.getAll('user'))
      ).subscribe((data) => {
        this.clearUser(+data);
      });
    }else if(this.typeWarn == EWarn.ERROR_ABOUT_UNIVERSAL) this.clearAll();
  }

  /**
   * Очищаем данные о предупреждениях по пользователю
   */
  clearUser(userId: number): void {
    this.warnService.clearLastWarnsByUser(userId).subscribe(result => {
      this.loadService.isLoad();
      this.messageService.add({severity:'success', summary:'Ура', detail:'Очистка прошла успешно'});
      this.loadUsersWarn(userId);
      this.closeAndReload();
    }, error => {
      this.messageService.add({severity:'error', summary:'Ошибка'});
    })
  }

  /**
   * Очищаем данные о всех предупреждениях
   */
  clearAll(): void {
    this.warnService.clearAllWarn().subscribe(result => {
      this.loadService.isLoad();
      this.messageService.add({severity:'success', summary:'Ура', detail:'Очистка прошла успешно'});
      this.closeAndReload();
    }, error => {
      this.messageService.add({severity:'error', summary:'Ошибка'});
    })
  }

  /**
   * Метод закрывает и обновляет страницу
   */
  closeAndReload() {
    setTimeout(() => {
      window.location.reload();
    },1500)
  }

}
