import { Component, OnInit } from '@angular/core';


/**
 * В данный класс относим все что нужно для администрирования
 */
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
