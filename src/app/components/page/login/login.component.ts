import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../services/auth.service";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";
import {TokenStorageService} from "../../../services/token-storage.service";
import {LoadService} from "../../../services/load.service";
import {AdsComponent} from "../../ads/ads/ads.component";

declare var grecaptcha: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  token: string|undefined;

  constructor(public loadService: LoadService,
              private authService: AuthService,
              private messageService: MessageService,
              private fb: FormBuilder,
              private tokenStorage: TokenStorageService,
              private router: Router) {
    if (this.tokenStorage.getUser()) {
      this.router.navigate(['main']);
    }
  }

  /**
   * Получаем значение токена
   */
  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
    this.token = captchaResponse;
  }

  ngOnInit(): void {
    this.loginForm = this.createLoginForm();
  }

  createLoginForm(): FormGroup {
    return this.fb.group({
      username: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required])],
    });
  }

  submit(): void {

    if(this.loginForm.invalid || this.token == undefined){
      console.log(this.loginForm.errors);
      this.messageService.add({severity:'warn', summary:'Внимание', detail:'Введите корректно поля для входа'});
      return;
    }

    this.loadService.loading();

    this.authService.login({
      username: this.loginForm.value.username,
      password: this.loginForm.value.password,
      recaptcha: this.token,
    }).subscribe(data => {
      console.log(data);

      this.tokenStorage.saveToken(data.token);
      this.tokenStorage.saveUser(data.user);
      this.router.navigate(['/on']).then(() => {
        window.location.reload();
      });
    }, error => {
      console.log(error);
      this.messageService.add({severity:'error', summary:'Ошибка', detail:'Данные не верны'});
      this.loadService.isLoad();
      grecaptcha.reset();
    });
  }

}
