import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../services/user/user.service";
import {RoleService} from "../../../services/role.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.css']
})
export class MasterComponent implements OnInit {

  constructor(private userService: UserService,
              private router: Router,
              public roleService: RoleService) { }

  ngOnInit(): void {
    this.routerCheck();
  }

  /**
   * Если нужно перекинуть пользователя
   * по необходимости
   */
  routerCheck(): void{
    if(this.userService.currentUserBox != null && this.userService.currentUserBox.mainClip != null){
      this.router.navigate(['/clip/' + this.userService.currentUserBox.mainClip.id])
    } else this.router.navigate(['/'])
  }

}
