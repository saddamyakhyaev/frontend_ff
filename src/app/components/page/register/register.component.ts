import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../services/auth.service";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";
declare var grecaptcha: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;
  token: string|undefined;

  constructor(private authService: AuthService,
              private messageService: MessageService,
              private fb: FormBuilder,
              private router: Router) { }

  ngOnInit(): void {
    this.registerForm = this.createRegisterForm();
  }

  createRegisterForm(): FormGroup {
    return this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      name: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
    });
  }

  /**
   * Получаем значение токена
   */
  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
    this.token = captchaResponse;
  }

  submit(): void {

    if(this.registerForm.invalid || this.token == undefined){
      console.log(this.registerForm.errors);
      this.messageService.add({severity:'warn', summary:'Внимание', detail:'Введите корректно поля для регистрации'});
      return;
    }
    this.authService.register({
      email: this.registerForm.value.email,
      name: this.registerForm.value.name,
      password: this.registerForm.value.password,
      recaptcha: this.token,
    }).subscribe(data => {
      console.log(data);
      this.messageService.add({severity:'success', summary:'Ура', detail:'Регистрация прошла успешно'});
      this.router.navigate(["/login"])
    }, error => {
      this.messageService.add({severity:'error', summary:'Ошибка', detail:'Попробуйте чуть позже'});
      grecaptcha.reset();
    });
  }

}
