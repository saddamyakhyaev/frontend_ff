import {Component, Input, OnInit} from '@angular/core';
import {RoleService} from "../../../services/role.service";
import {DeviceService} from "../../../services/device.service";
import {User} from "../../../models/User";
import {UserService} from "../../../services/user/user.service";
import {Router} from "@angular/router";
import {UserMethodService} from "../../../services/user/user-method.service";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(public roleService: RoleService,
              public userMethodService: UserMethodService,
              private router: Router,
              public deviceService: DeviceService) { }

  ngOnInit(): void {
    this.routerCheck();
  }

  /**
   * Если нужно перекинуть пользователя
   * по необходимости
   */
  routerCheck(): void{
    // if(this.userMethodService.getUserMainClip() != null){
    //   this.router.navigate(['/clip/' + this.userMethodService.getUserMainClip()?.id])
    // }
  }

}
