export interface ClipRequest {
  id: number;
  eventId: number | null;
  eventTimeId: number | null;
  eventDateId: number | null;
}
