import {Contact} from "./Contact";

export interface EnvironmentInterface {
  production: boolean;

  name: string,
  apiUrl: string;
  recaptcha_siteKey: string;
  theme_default: string;
  contacts: Contact[];
}
