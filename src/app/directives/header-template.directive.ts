import {Directive, TemplateRef} from '@angular/core';

@Directive({
  selector: '[appHeaderTemplate]'
})
export class HeaderTemplateDirective {

  get Template(): TemplateRef<any> {
    return this.template;
  }

  constructor(private readonly template: TemplateRef<any>) {}


}
