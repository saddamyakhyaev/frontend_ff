import {CUSTOM_ELEMENTS_SCHEMA, Inject, Injectable, InjectionToken, NgModule, Optional} from '@angular/core';
import {BrowserModule, Title} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ClipBodyComponent} from './components/clip/clip-body/clip-body.component';
import {EventBodyComponent} from './components/event/event-body/event-body.component';
import {IndexComponent} from './components/page/index/index.component';
import {MasterComponent} from './components/page/master/master.component';
import {LeftBodyComponent} from './components/navigation/left-body/left-body.component';
import {FindComponent} from './components/navigation/find/find.component';
import {ClipLinkComponent} from './components/clip/clip-link/clip-link.component';
import {LoginComponent} from './components/page/login/login.component';
import {RegisterComponent} from './components/page/register/register.component';
import {InputTextModule} from "primeng/inputtext";
import {ToastModule} from "primeng/toast";
import {InputNumberModule} from "primeng/inputnumber";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {CardModule} from "primeng/card";
import {DialogModule} from "primeng/dialog";
import {FileUploadModule} from "primeng/fileupload";
import {ButtonModule} from "primeng/button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {ConfirmationService, MessageService} from "primeng/api";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TokenStorageService} from "./services/token-storage.service";
import {authInterceptorProviders} from "./helper/auth-interceptor.service";
import {authErrorInterceptorProviders} from "./helper/error-interceptor.service";
import { ProfileComponent } from './components/user/profile/profile.component';
import {SidebarModule} from "primeng/sidebar";
import {LoadService} from "./services/load.service";
import {MenuModule} from "primeng/menu";
import {TieredMenuModule} from "primeng/tieredmenu";
import { MainMenuComponent } from './components/navigation/main-menu/main-menu.component';
import { ClipCreateComponent } from './components/clip/clip-create/clip-create.component';
import {InputTextareaModule} from "primeng/inputtextarea";
import {DropdownModule} from "primeng/dropdown";
import { EventCreateComponent } from './components/event/event-create/event-create.component';
import { EventSeasonCreateComponent } from './components/event/season/event-season-create/event-season-create.component';
import {CalendarModule} from "primeng/calendar";
import { EventLimitCreateComponent } from './components/event/limit/event-limit-create/event-limit-create.component';
import { EventLimitViewComponent } from './components/event/limit/event-limit-view/event-limit-view.component';
import { EventSeasonViewComponent } from './components/event/season/event-season-view/event-season-view.component';
import {ConfirmDialogModule} from "primeng/confirmdialog";
import { EventViewComponent } from './components/event/event-view/event-view.component';
import { EventTimeCreateComponent } from './components/event/time/event-time-create/event-time-create.component';
import { EventTimeCreatePageComponent } from './components/event/time/event-time-create-page/event-time-create-page.component';
import {ToggleButtonModule} from "primeng/togglebutton";
import { ClipWeekComponent } from './components/clip/clip-week/clip-week.component';
import { ClipViewComponent } from './components/clip/clip-view/clip-view.component';
import { ActionsPageComponent } from './components/navigation/actions-page/actions-page.component';
import { HeaderTemplateDirective } from './directives/header-template.directive';
import {CookieService} from "ngx-cookie-service";
import { NewsViewComponent } from './components/news/news-view/news-view.component';
import { NewsComponent } from './components/news/news/news.component';
import { NewsCreateComponent } from './components/news/news-create/news-create.component';
import {CheckboxModule} from "primeng/checkbox";
import {EditorModule} from "primeng/editor";
import { AdsViewComponent } from './components/ads/ads-view/ads-view.component';
import { AdsCreateComponent } from './components/ads/ads-create/ads-create.component';
import { AdsComponent } from './components/ads/ads/ads.component';
import { MarkViewComponent } from './components/mark/mark-view/mark-view.component';
import { MarkCreateComponent } from './components/mark/mark-create/mark-create.component';
import { MarkLinkComponent } from './components/mark/mark-link/mark-link.component';
import {MultiSelectModule} from "primeng/multiselect";
import { ClipListComponent } from './components/clip/clip-list/clip-list.component';
import { DonatViewComponent } from './components/donat/donat-view/donat-view.component';
import { DonatCreateComponent } from './components/donat/donat-create/donat-create.component';
import { ChartLineComponent } from './components/another/chart-line/chart-line.component';
import { environment } from 'src/environments/environment.prod';
import {RECAPTCHA_SETTINGS, RecaptchaFormsModule, RecaptchaModule, RecaptchaSettings} from "ng-recaptcha";
import { WarnViewComponent } from './components/warn/warn-view/warn-view.component';
import { WarnCreateComponent } from './components/warn/warn-create/warn-create.component';
import { AdminViewComponent } from './components/admin/admin-view/admin-view.component';
import { WarnBlockComponent } from './components/warn/warn-block/warn-block.component';
import {WarnComponent} from "./components/page/warn/warn.component";
import {NewsPageComponent} from "./components/page/news/news-page.component";
import { OtherComponent } from './components/other/other.component';


export const ENVIRONMENT = new InjectionToken<{ [key: string]: any }>('environment');

//ng serve --host 0.0.0.0

@NgModule({
  declarations: [
    AppComponent,
    ClipBodyComponent,
    EventBodyComponent,
    IndexComponent,
    MasterComponent,
    LeftBodyComponent,
    FindComponent,
    ClipLinkComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    MainMenuComponent,
    ClipCreateComponent,
    EventCreateComponent,
    EventSeasonCreateComponent,
    EventLimitCreateComponent,
    EventLimitViewComponent,
    EventSeasonViewComponent,
    EventViewComponent,
    EventTimeCreateComponent,
    EventTimeCreatePageComponent,
    ClipWeekComponent,
    ClipViewComponent,
    ActionsPageComponent,
    HeaderTemplateDirective,
    NewsViewComponent,
    NewsComponent,
    NewsCreateComponent,
    AdsViewComponent,
    AdsCreateComponent,
    AdsComponent,
    MarkViewComponent,
    MarkCreateComponent,
    MarkLinkComponent,
    ClipListComponent,
    DonatViewComponent,
    DonatCreateComponent,
    ChartLineComponent,
    WarnViewComponent,
    WarnCreateComponent,
    AdminViewComponent,
    WarnBlockComponent,
    WarnComponent,
    NewsPageComponent,
    OtherComponent
  ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        AppRoutingModule,
        InputTextModule,
        ProgressSpinnerModule,
        ToastModule,
        InputNumberModule,
        FileUploadModule,
        DialogModule,
        CardModule,
        ButtonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SidebarModule,
        TieredMenuModule,
        InputTextareaModule,
        DropdownModule,
        CalendarModule,
        ConfirmDialogModule,
        ToggleButtonModule,
        CheckboxModule,
        EditorModule,
        MultiSelectModule,
        RecaptchaFormsModule,
        RecaptchaModule
    ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [
    { provide: 'Window',  useValue: window },
    { provide: ENVIRONMENT, useValue: environment },
    {provide: RECAPTCHA_SETTINGS, useValue: {siteKey: environment.recaptcha_siteKey} as RecaptchaSettings},
    authInterceptorProviders,
    authErrorInterceptorProviders,
    MessageService,
    TokenStorageService,
    LoadService,
    ConfirmationService,
    CookieService,
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
