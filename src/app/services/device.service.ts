import {Inject, Injectable, Renderer2, RendererFactory2} from '@angular/core';
import {DOCUMENT} from "@angular/common";
import {Theme} from "../models-client/Theme";
import {EnvironmentService} from "./environment.service";


/**
 * Данный сервис отвечает за то, что связано
 * с адаптацией между ПК и мобильным устройством
 *
 * А так же за дизайн
 */
@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  /**
   * Истина - ПК девайс
   * Ложь - Мобильное устройство
   */
  pc: boolean = true;

  /**
   * Данные о шаблонах стилей
   */
  themes: Theme[] = [];

  /**
   * Выбранный стиль
   */
  selectTheme: Theme = {} as Theme;

  private renderer: Renderer2;

  constructor(@Inject('Window') private window: Window,
              @Inject(DOCUMENT) private document: Document,
              private environmentService: EnvironmentService,
              private rendererFactory: RendererFactory2) {

    this.renderer = rendererFactory.createRenderer(null, null);
    this.startCheck();
    this.setThemes();
  }

  /**
   * Устанавливаем темы
   */
  setThemes(): void {
    this.themes = [
      {name: "colorBlue", colorIcon: "white"},
      {name: "colorWhiteBlue", colorIcon: "black"}
    ];

    this.themes.forEach(theme => {
      if(theme.name === this.environmentService.getValue("theme_default")) this.selectTheme = theme;
    })
  }

  getTheme(): Theme {
    return this.selectTheme;
  }

  /**
   * Определяем девайс и делаем начальные шаги
   */
  startCheck(): void {
    if(this.window.innerWidth < 800){
      this.pc = false;
      this.renderer.addClass(document.body, 'mobil-device-master');
    }else{
      this.pc = true;
      this.renderer.addClass(document.body, 'pc-device-master');
    }

  }

  public isPC(): boolean {
    return this.pc;
  }

  public getWidth(): number {
    return this.window.innerWidth;
  }

  public getBreakPoints(): any {
    return {'620px': {width: '90%', right: '5%', left: '5%'}};
  }

  public getBreakPointsPage(): any {
    return {'960px': '80vw', '640px': '90vw'};
  }
}
