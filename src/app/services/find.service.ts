import { Injectable } from '@angular/core';
import {User} from "../models/User";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {EnvironmentService} from "./environment.service";

@Injectable({
  providedIn: 'root'
})
export class FindService {

  URL_API: string;

  constructor(private http: HttpClient,
              private environmentService:EnvironmentService) {
    this.URL_API =  environmentService.getValue("apiUrl") + '/api/find/';
  }

  find(str: string): Observable<any> {
    return this.http.get(this.URL_API + str);
  }
}
