import { Injectable } from '@angular/core';
import {UserService} from "../user/user.service";
import {ClipService} from "./clip.service";
import {Clip} from "../../models/clip/Clip";
import {Event} from "../../models/event/Event";
import {ClipType} from "../../models/clip/ClipType";
import {DeviceService} from "../device.service";
import {EventBody} from "../../models/event/EventBody";
import {ClipRequest} from "../../models-client/ClipRequest";

@Injectable({
  providedIn: 'root'
})
export class ClipMethodService {

  /**
   * Закрепы созданные пользователем
   */
  clipsCreating: Clip[] = [];

  /**
   * Закрепы сохраненные на всякий случай
   */
  clipsCash: Clip[] = [];




  constructor(private userService: UserService,
              private deviceService: DeviceService,
              private clipService: ClipService) { }

  /**
   * Проверяем по ID закрепа присутствует ли он в списке избранных
   * @param clipId - ID закрепа
   */
  isFavorite(clipId: number): boolean {

    if(this.userService.currentUserBox == null) return false;
    for(let i = 0; i < this.userService.currentUserBox.favouritesClips.length; i++){
      if(this.userService.currentUserBox.favouritesClips[i].id == clipId) return true;
    }
    return false;
  }

  /**
   * Загрузка важных данных
   */
  load(method?: any): void {
    this.userService.loadCurrentUser().subscribe(result => {
      this.userService.setUserBoxCurrent(result.userBox);
      this.loadClipsCreated();

      if(method) method();
    });
  }

  /**
   * Загружаем закрепы созданные
   * пользователем
   */
  loadClipsCreated(): void {
    this.clipService.getClipsCreatingByUser(this.userService.currentUser.id).subscribe(result => {
      this.clipsCreating = result;
      this.clipsCreating = this.clipsCreating.reverse();
    })

  }

  /**
   * Возвращаем тип закрепа по ID из уже загруженных типов
   * @param id
   */
  getClipTypesOfId(id: number): ClipType {
    for(var i = 0; i < this.clipService.clipTypes.length; i++){
      if(this.clipService.clipTypes[i].id == id) return this.clipService.clipTypes[i];
    }
    return {name: "", icon: ""};
  }

  /**
   * Нужно подготовить типы закрепов для вывода
   */
  settingClipType(clips: ClipType[]): ClipType[] {

    clips.forEach(clipType => {
      if(clipType.icon.indexOf("http") == -1) {
        clipType.icon = `/assets/img/${this.deviceService.getTheme().colorIcon}/${clipType.icon}`;
      }
    })
    return clips;
  }

  /**
   * Из события убираем то, что и так ясно что привязанно к событию
   */
  getClipsWithoutClipArgument(eventBody: EventBody): Clip[] {
    let clipsReturn: Clip[] = [];
    eventBody.event.clips.forEach(clip => clip.id != eventBody.clip.id? clipsReturn.push(clip):'');

    return clipsReturn;
  }

  /**
   * Проверяем по ID закрепа является ли он главным закрепом
   * @param clipId - ID закрепа
   */
  isMain(clipId: number): boolean {
    if(this.userService.currentUserBox == null) return false;
    if(this.userService.currentUserBox.mainClip == null) return false;

    if(this.userService.currentUserBox.mainClip.id == clipId) return true;
    else return false;
  }

  /**
   * Преобразует запрос в нужный объект данных
   * @param argsRequest
   */
  getClipRequestByArgs(argsRequest: string): ClipRequest {

    if(argsRequest.indexOf("_") == -1){
      return {id: Number(argsRequest), eventId: null, eventTimeId: null, eventDateId: null}
    }else {
      let args: string[] = argsRequest.split("_");
      return {id: Number(args[0]), eventId: Number(args[1]), eventTimeId: Number(args[2]), eventDateId: Number(args[3])}
    }
  }


  /**
   * Возвращаем тело запроса с пустыми значениями кроме id
   */
  getClipRequestOfNull(clipRequest: ClipRequest): ClipRequest  {

    clipRequest.eventId = null;
    clipRequest.eventTimeId = null;
    clipRequest.eventDateId = null;

    return clipRequest;
  }
}
