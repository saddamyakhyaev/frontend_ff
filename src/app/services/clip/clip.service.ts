import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ClipType} from "../../models/clip/ClipType";
import {Observable} from "rxjs";
import {EnvironmentService} from "../environment.service";

@Injectable({
  providedIn: 'root'
})
export class ClipService {

  URL_API: string;

  public clipTypes: ClipType[];

  constructor(private http: HttpClient,
              private environmentService:EnvironmentService) {
    this.URL_API =  environmentService.getValue("apiUrl") + '/api/clip/';
  }


  /**
   * Получить все типы закрепов
   */
  getClipTypes(): Observable<any> {
    return this.http.get(this.URL_API + "types/all");
  }


  /**
   * Создать закреп
   * @param data результат
   */
  createClip(data: any): Observable<any> {
    return this.http.post(this.URL_API + "create", data);
  }

  /**
   * Возвращает нужный закреп
   * @param id закрепа
   */
  getClip(id: number): Observable<any> {
    return this.http.get(this.URL_API + id);
  }

  /**
   * Возвращает все закрепы созданные пользователем
   * @param id закрепа
   */
  getClipsCreatingByUser(userId: number): Observable<any> {
    return this.http.get(this.URL_API + 'get/creating/' + userId);
  }

  /**
   * Добавляем закреп в список избранных
   * @param id закрепа
   */
  addInFavouritesClip(id: number): Observable<any> {
    return this.http.get(this.URL_API + 'add/favourite/' + id);
  }

  /**
   * Удаляем закреп из списка избранных
   * @param id закрепа
   */
  deleteInFavouritesClip(id: number): Observable<any> {
    return this.http.get(this.URL_API + 'delete/favourite/' + id);
  }


  /**
   * Добавляем главный закреп
   * @param id закрепа
   */
  addMainClip(id: number): Observable<any> {
    return this.http.get(this.URL_API + 'add/main/' + id);
  }

}
