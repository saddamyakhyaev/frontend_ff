import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {EnvironmentService} from "../environment.service";
import {Observable} from "rxjs";
import {Warn} from "../../models/warn/Warn";

@Injectable({
  providedIn: 'root'
})
export class WarnService {

  URL_API: string;

  lastWarn: Warn[] = [];

  constructor(private http: HttpClient,
              private environmentService:EnvironmentService) {
    this.URL_API = environmentService.getValue("apiUrl") + '/api/warn/';

  }

  /**
   * Отправляем предупреждение на сервер
   */
  sendWarn(data: any): Observable<any> {
    return this.http.post(this.URL_API + "send", data);
  }

  /**
   * Получаем последние ошибки
   */
  getLastWarns(): Observable<any> {
    return this.http.get(this.URL_API + "last");
  }


  /**
   * Очищаем предупреждения по пользователю
   */
  clearAllWarn(): Observable<any> {
    return this.http.get(this.URL_API + "clear");
  }

  /**
   * Получаем последние ошибки по пользователю
   */
  getLastWarnsByUser(userId: number): Observable<any> {
    return this.http.get(this.URL_API + "user/" + userId);
  }

  /**
   * Очищаем предупреждения по пользователю
   */
  clearLastWarnsByUser(userId: number): Observable<any> {
    return this.http.get(this.URL_API + "user/clear/" + userId);
  }
}
