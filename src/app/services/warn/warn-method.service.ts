import { Injectable } from '@angular/core';
import {WarnService} from "./warn.service";
import {Warn} from "../../models/warn/Warn";
import {EType} from "../../models/enum/EType";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class WarnMethodService {

  constructor(private warnService: WarnService,
              private router: Router) { }


  /**
   * Список шаблонов предупреждений для закрепов
   */
  getPatternsWarnsForClips(): string[] {
    return [
      "Рассписание ошибочно",
      "Группа указана не верно"
    ]
  }

  /**
   * Список шаблонов предупреждений для событий
   */
  getPatternsWarnsForEvents(): string[] {
    return [
      "Данного события нет в рассписании",
      "В тексте события есть ошибка"
    ]
  }

  /**
   * Возвращаем количество ошибок у пользователя
   * @param userId
   */
  getCountWarnsByUser(userId: number): number {
    let count: number = 0;
    this.warnService.lastWarn.forEach(warn => {
      if(warn.userId) if(warn.userId == userId) count++;
    })

    console.log("c: " + count)
    return count;
  }

  getObjectNameByType(warn: Warn): string {
    if(warn.objectType == EType.TYPE_CLIP.valueOf()) {
      return "Закреп";
    }if(warn.objectType == EType.TYPE_EVENT.valueOf()) {
      return "Событие";
    }
    return "Не известно"
  }

  getLinkOnObject(warn: Warn): string {
    if(warn.objectType == EType.TYPE_CLIP.valueOf()) {
      return this.router.createUrlTree(['/clip', warn.objectId]).toString();
    }if(warn.objectType == EType.TYPE_EVENT.valueOf()) {
      return "Событие";
    }
    return "/"
  }
}
