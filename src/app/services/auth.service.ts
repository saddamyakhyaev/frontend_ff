  import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {TokenStorageService} from "./token-storage.service";
import {Router} from "@angular/router";
import {EnvironmentService} from "./environment.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  URL_API: string;

  constructor(private http: HttpClient,
              private tokenStorage: TokenStorageService,
              private router: Router,
              private environmentService:EnvironmentService) {
    this.URL_API =  environmentService.getValue("apiUrl") + '/api/auth/';
  }

  public login(data: any): Observable<any> {
    return this.http.post(this.URL_API + 'signin', data);
  }

  public register(user: any): Observable<any> {
    return this.http.post(this.URL_API + 'signup', {
      email: user.email,
      name: user.name,
      password: user.password,
      recaptcha: user.recaptcha
    });
  }

  public logOut(): void {
    this.tokenStorage.logOut();
    window.location.reload();
  }
}
