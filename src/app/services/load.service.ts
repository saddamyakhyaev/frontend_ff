import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadService {

  public load: boolean = true;
  public loadLazy: boolean = true;
  public loadNumber: number = 0;

  constructor() { }

  /**
   * Загрузка
   */
  loading(number?: number): void {

    if(number) this.loadNumber = number;
    else this.load = false;
  }


  /**
   * Ленивая загрузка
   */
  loadingLazy(): void {
    this.loadLazy = false;
  }

  /**
   * Загрузка окончена
   */
  isLoad(): void {
    this.load = true;
    this.loadLazy = true;
    this.loadNumber = 0;
  }

  /**
   * Статус
   */
  status(): boolean{
    return this.load;
  }

  /**
   * Статус ленивой загрузки
   */
  statusLazy(): boolean{
    return this.loadLazy;
  }



}
