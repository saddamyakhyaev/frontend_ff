import { Injectable } from '@angular/core';
import {DonatService} from "./donat.service";
import {Clip} from "../../models/clip/Clip";

@Injectable({
  providedIn: 'root'
})
export class DonatMethodService {

  constructor(public donatService: DonatService) {

  }


  isMaxDonate(clip: Clip): boolean {
    if(this.donatService.allDonates.length > 0)
      if(this.donatService.allDonates[0].clip.id == clip.id) return true;

    return false
  }
}
