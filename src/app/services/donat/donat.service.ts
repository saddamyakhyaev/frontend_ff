import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Donat} from "../../models/donat/Donat";
import {EnvironmentService} from "../environment.service";

@Injectable({
  providedIn: 'root'
})
export class DonatService {

  URL_API: string;
  /**
   * Список всех донатов
   */
  allDonates: Donat[] = [];


  constructor(private http: HttpClient,
              private environmentService:EnvironmentService) {

    this.URL_API =  environmentService.getValue("apiUrl") + '/api/donat/';
  }

  /**
   * Создать закреп
   * @param data результат
   */
  createOrAddDonat(data: any): Observable<any> {
    return this.http.post(this.URL_API + "add", data);
  }

  /**
   * Получить всю информацию о донатах
   */
  getAllDonates(): Observable<any> {
    return this.http.get(this.URL_API + "all");
  }
}
