import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {EventSeason} from "../../models/event/EventSeason";
import {EventLimit} from "../../models/event/EventLimit";
import {EnvironmentService} from "../environment.service";


/**
 * Сервис для работы событиями и
 * со всем что связанно с ними
 * (по большей части связь клиент-сервер)
 */
@Injectable({
  providedIn: 'root'
})
export class EventService {

  URL_API: string;

  /**
   * Все сезоны событий
   */
  allEventSeasons: EventSeason[];

  /**
   * Все лимиты событий
   */
  allEventLimits: EventLimit[];


  constructor(private http: HttpClient,
              private environmentService:EnvironmentService) {
    this.URL_API =  environmentService.getValue("apiUrl") + '/api/event/';
  }

  /**
   * Получаем событие по id
   * @param evetId id события
   */
  getEventById(evetId: number): Observable<any> {
    return this.http.get(this.URL_API + evetId);
  }

  /**
   * Получаем все события закрепа
   * @param clipId id закрепа
   */
  getEventsToClip(clipId: number): Observable<any> {
    return this.http.get(this.URL_API + 'all/' + clipId);
  }

  /**
   * Создаём сезон событий
   * @param data - объект для создания
   */
  createEventSeason(data: any): Observable<any> {
    return this.http.post(this.URL_API + "season/create", data);
  }

  /**
   * Получаем все сезоны событий
   */
  getAllEventSeasons(): Observable<any> {
    return this.http.get(this.URL_API + 'season/all');
  }

  /**
   * Удаляем сезон
   */
  deleteEventSeason(eventSeasonId: number): Observable<any> {
    return this.http.post(this.URL_API + "season/delete/" + eventSeasonId, {});
  }

  /**
   * Активируем сезон
   */
  activeEventSeason(eventSeasonId: number): Observable<any> {
    return this.http.post(this.URL_API + "season/active/" + eventSeasonId, {});
  }

  /**
   * Блокируем сезон
   */
  blockEventSeason(eventSeasonId: number): Observable<any> {
    return this.http.post(this.URL_API + "season/block/" + eventSeasonId, {});
  }

  /**
   * Создаём границы событий
   * @param data - объект для создания
   */
  createEventLimit(data: any): Observable<any> {
    console.log(data)
    return this.http.post(this.URL_API + "limit/create", data);
  }

  /**
   * Получаем все границы событий
   */
  getAllEventLimits(): Observable<any> {
    return this.http.get(this.URL_API + 'limit/all');
  }

  /**
   * Удаляем лимит
   */
  deleteEventLimit(eventLimitId: number): Observable<any> {
    return this.http.post(this.URL_API + "limit/delete/" + eventLimitId, {});
  }


  /**
   * Создаём событие
   * @param data - объект для создания
   */
  createEvent(data: any): Observable<any> {
    return this.http.post(this.URL_API + "create", data);
  }

  /**
   * Получаем все события закрепа по id
   */
  getAllEvent(clipId: number): Observable<any> {
    return this.http.get(this.URL_API + 'all/' + clipId);
  }

  /**
   * Получаем все события закрепа по id
   */
  getAllEventBySeason(clipId: number, seasonId: number): Observable<any> {
    return this.http.get(this.URL_API + `all/${clipId}/${seasonId}`);
  }

  /**
   * Удаляем соыбтие
   */
  deleteEvent(eventId: number): Observable<any> {
    return this.http.post(this.URL_API + "delete/" + eventId, {});
  }



}
