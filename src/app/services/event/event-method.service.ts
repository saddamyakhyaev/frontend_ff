import { Injectable } from '@angular/core';
import {EventLimit} from "../../models/event/EventLimit";
import {EventTime} from "../../models/event/EventTime";
import {EventBody} from "../../models/event/EventBody";
import {EventService} from "./event.service";
import {SubscriptionService} from "../subscription.service";
import {ClipRequest} from "../../models-client/ClipRequest";
import {Event} from "../../models/event/Event";
import {Clip} from "../../models/clip/Clip";


/**
 * Данный сервис создан для задач
 * связанных с событиями
 */
@Injectable({
  providedIn: 'root'
})
export class EventMethodService {

  constructor(private eventService: EventService,
              private readonly subscriptionService: SubscriptionService) {

  }


  /**
   * Возвращаем лимит если время у события соответствует хоть одному лимиту
   */
  getLimitByEventBody(eventBody: EventBody): EventLimit | null {
    let eventTime: EventTime | null = this.getEventTimeByEventBody(eventBody);
    if(eventTime == null) return null;

    for(let i = 0; i < this.eventService.allEventLimits.length; i++){
      let limit: EventLimit = this.eventService.allEventLimits[i];
      if(limit.timeTo === eventTime.timeTo && limit.timeFrom === eventTime.timeFrom){
        return limit;
      }
    }

    return null;
  }

  /**
   * Возвращаем Время события отталкивась от EventBody, и переменной idEventTime
   */
  getEventTimeByEventBody(eventBody: EventBody): EventTime | null{
    for(let i = 0; i < eventBody.event.dates.length; i++){
      if(eventBody.event.dates[i].id == eventBody.idEventTime) return eventBody.event.dates[i]
    }
    return null;
  }

  /**
   * Возвращаем Время события отталкивась от EventBody, и переменной idEventTime
   */
  getEventDateByEventBody(eventBody: EventBody): Date | string | null{

    let eventTime: EventTime | null = this.getEventTimeByEventBody(eventBody);
    if(eventTime?.dates){
      return eventTime.dates[eventBody.idDates];
    }

    return null;
  }

  /**
   * Возвращаем тело события из обьекта запроса и подходящего события
   */
  getEventBodyByClipRequest(clip: Clip, event: Event, clipRequest: ClipRequest): EventBody | null {

    if(clipRequest.eventDateId == null) return null;
    for(let i = 0; i < event.dates.length; i++){
      if(event.dates[i].id == clipRequest.eventTimeId){
        if(event.dates[i]?.dates)
          if(event.dates[i]?.dates[clipRequest.eventDateId] != undefined){
              return {clip: clip,
                event: event,
                time: event.dates[i],
                day: event.dates[i]?.dates[clipRequest.eventDateId],
                idEventTime: clipRequest.eventTimeId,
                idDates: clipRequest.eventDateId}
          }
      }
    }

    return null;
  }




}
