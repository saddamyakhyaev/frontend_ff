import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {News} from "../../models/news/News";
import {EnvironmentService} from "../environment.service";

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  URL_API: string;

  /**
   * Загруженные новости
   */
  allNews: News[] = [];

  constructor(private http: HttpClient,
              private environmentService:EnvironmentService) {
    this.URL_API = environmentService.getValue("apiUrl") + '/api/news/';
  }

  /**
   * Создаём новость
   * @param data - объект для создания
   */
  createNews(data: any): Observable<any> {
    return this.http.post(this.URL_API + "create", data);
  }

  /**
   * Получаем все новости
   */
  getAllActiveNews(): Observable<any> {
    return this.http.get(this.URL_API + 'all');
  }

  /**
   * Получаем тело новости
   * @param newsId id новости
   */
  getNewsBody(newsId: number): Observable<any> {
    return this.http.get(this.URL_API + 'body/' + newsId);
  }

  /**
   * Закрепляем новость
   * @param newsId id новости
   */
  pinnedNews(newsId: number): Observable<any> {
    return this.http.get(this.URL_API + 'is-pinned/' + newsId);
  }

  /**
   * Открепляем новость
   * @param newsId id новости
   */
  pinnedNotNews(newsId: number): Observable<any> {
    return this.http.get(this.URL_API + 'is-not-pinned/' + newsId);
  }

  /**
   * Восстанавливаем новость
   * @param newsId id новости
   */
  activeNews(newsId: number): Observable<any> {
    return this.http.get(this.URL_API + 'is-active/' + newsId);
  }

  /**
   * Останавливаем новость
   * @param newsId id новости
   */
  activeNotNews(newsId: number): Observable<any> {
    return this.http.get(this.URL_API + 'is-not-active/' + newsId);
  }

  /**
   * Сохраняем последнюю новость, что видел пользователь
   * @param newsId id новости
   */
  setUserLastNews(newsId: number): Observable<any> {
    return this.http.get(this.URL_API + 'last-news/' + newsId);
  }
}
