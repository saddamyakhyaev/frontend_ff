import { Injectable } from '@angular/core';
import {News} from "../../models/news/News";
import {NewsService} from "./news.service";
import {Router} from "@angular/router";


/**
 * Данный сервис создан для задач
 * связанных с новостями
 */

@Injectable({
  providedIn: 'root'
})
export class NewsMethodService {


  constructor(private newsService: NewsService,
              private router: Router) { }

  /**
   * Возвращаем ссылка на новость
   */
  getLinkNews(news: News): string {
    const url = this.router.createUrlTree(['/news', news.id])
    return window.location.host + url.toString()
  }

}
