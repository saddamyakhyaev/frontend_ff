import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../../models/User";
import {ERole} from "../../models/enum/ERole";
import {UserBox} from "../../models/UserBox";
import {EnvironmentService} from "../environment.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  URL_API: string;

  /**
   * Здесь храниться объект пользовательских данных авторизованного пользователя
   */
  currentUser: User;

  /**
   * Здесь хранятся личные данные авторизованного пользователя
   */
  public currentUserBox: UserBox | null = null;

  /**
   * Имя авторизованного пользователя
   */
  public currentNameUser: string;

  constructor(private http: HttpClient,
              private environmentService:EnvironmentService) {
    this.URL_API = environmentService.getValue("apiUrl") + '/api/user/';
  }

  /**
   * Установить текущего пользователя
   */
  setUserCurrent(user: any) {
    this.currentUser = user;
  }
  /**
   * Установить личные данные текущего пользователя
   */
  setUserBoxCurrent(userBox: any) {
    this.currentUserBox = userBox;

    if(this.currentUserBox?.mainClip != null) {
      if(this.currentUserBox?.favouritesClips) this.currentUserBox.favouritesClips = this.currentUserBox.favouritesClips.reverse();
      this.currentUserBox?.favouritesClips.sort((x, y) => {
        if (y.id == this.currentUserBox?.mainClip.id) return 1;
        else if (x.id == this.currentUserBox?.mainClip.id) return -1;
        else return 0
      })
    }
  }


  /**
   * Получить авторизованного пользователя с сервера
   */
  loadCurrentUser(): Observable<any> {
    return this.http.get(this.URL_API);
  }

  /**
   * Обрабоать полученные данные
   */
  currentUserSetResult(result: any): void {
    this.setUserCurrent(result);
    this.setUserBoxCurrent(result.userBox);
    this.currentNameUser = result.name;
  }



  /**
   * Установить роль для F-пользователя
   * @param userId пользовательский ID
   */
  setRoleUser(userId: number, role: string): Observable<any> {
    return this.http.get(this.URL_API + 'role/set/' + userId + '/' + role);
  }

  /**
   * Получить F-пользователя
   * @param userId пользовательский ID
   */
  getUser(userId: number): Observable<any> {
    return this.http.get(this.URL_API + 'get/' + userId);
  }

  /**
   * Блокируем пользователя
   * @param userId пользовательский ID
   */
  blockingUser(userId: number): Observable<any> {
    return this.http.post(this.URL_API + 'block-user/' + userId, {});
  }
  /**
   * Разблокировать пользователя
   * @param userId пользовательский ID
   */
  unBlockingUser(userId: number): Observable<any> {
    return this.http.post(this.URL_API + 'un-block-user/' + userId, {});
  }

  /**
   * Получить F-пользователя
   * @param userId пользовательский ID
   */
  getUsersIsModerator(): Observable<any> {
    return this.http.get(this.URL_API + 'get/moderators/');
  }
}
