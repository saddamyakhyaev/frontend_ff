import { Injectable } from '@angular/core';
import {UserBox} from "../../models/UserBox";
import {UserService} from "./user.service";
import {Clip} from "../../models/clip/Clip";
import {ERole} from "../../models/enum/ERole";


/**
 * Данный сервис создан для задач
 * связанных с пользовательскими данными
 */
@Injectable({
  providedIn: 'root'
})
export class UserMethodService {

  constructor(private userService: UserService) { }

  getUserMainClip(): Clip | null {
    if(this.userService.currentUserBox != null && this.userService.currentUserBox.mainClip != null) return this.userService.currentUserBox.mainClip;
    else return null;
  }

  getUserFavoritesClip(): Clip[] | null {
    if(this.userService.currentUserBox != null && this.userService.currentUserBox.favouritesClips != null) return this.userService.currentUserBox.favouritesClips;
    else return null;
  }

  getNameRoleById(id: number): string {
    switch (id) {
      case ERole.ROLE_ADMIN: return "Админ";
      case ERole.ROLE_MODERATOR_3: return "Модератор 3 уровня";
      case ERole.ROLE_MODERATOR_2: return "Модератор 2 уровня";
    }

    return "";
  }


}
