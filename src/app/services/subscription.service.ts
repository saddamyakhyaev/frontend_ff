import { Injectable } from '@angular/core';
import {Subject} from "rxjs";


/**
 * Данный сервис необходим для связи
 * с компонентом меню, для вызова функционала
 * данного компонента
 */
@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {


  /**
   * Все события для второстепенных задач
   */
  public other$ = new Subject<any>();

  /**
   * Все события для предупреждений
   */
  public warn$ = new Subject<any>();

  /**
   * Все события для донатов
   */
  public donat$ = new Subject<any>();

  /**
   * Все события для марок
   */
  public mark$ = new Subject<any>();

  /**
   * Все события для новостей
   */
  public news$ = new Subject<any>();

  /**
   * Все события для рекламы
   */
  public ads$ = new Subject<any>();

  /**
   * Новый вариант для всех событий событий :D
   */
  public event$ = new Subject<any>();

  /**
   * Новый вариант для всех событий событий :D
   */
  public clip$ = new Subject<any>();

  public eventCreate$ = new Subject<any>();
  public eventSeasonCreate$ = new Subject<any>();
  public eventLimitCreate$ = new Subject<any>();
  public clipCreate$ = new Subject<any>();
  public clipCreateSuccess$ = new Subject<any>();

  public createSuccess$ = new Subject<any>();

  constructor() { }

  /**
   * Что-то успешно добавлено
   * @param data передаваемое значение
   */
  public openCreateSuccess(data?: any) {
    this.createSuccess$.next(data);
  }

  /**
   * Вызов окна создания сезона событий
   * @param data передаваемое значение
   */
  public openEventSeasonCreateTable(data?: any) {
    this.eventSeasonCreate$.next(data);
  }


  /**
   * Вызов окна создания лимита событий
   * @param data передаваемое значение
   */
  public openEventLimitCreateTable(data?: any) {
    this.eventLimitCreate$.next(data);
  }

  /**
   * Вызов окна создания события
   * @param data передаваемое значение
   */
  public openEventCreateTable(data?: any) {
    this.eventCreate$.next(data);
  }


  /**
   * Вызов окна создания закрепа
   * @param data передаваемое значение
   */
  public openClipCreateTable(data?: any) {
    this.clipCreate$.next(data);
  }

  /**
   * Вызов действия связанного с событиями
   * Функциональность передается в теле переменной data
   * @param data передаваемое значение
   */
  public EEvent(data?: any) {
    this.event$.next(data);
  }

  /**
   * Событие новостей
   * @param data передаваемое значение
   */
  public ENews(data?: any) {
    this.news$.next(data);
  }

  /**
   * Событие реклам
   * @param data передаваемое значение
   */
  public EAds(data?: any) {
    this.ads$.next(data);
  }

  /**
   * Событие закрепов
   * @param data передаваемое значение
   */
  public EClip(data?: any) {
    this.clip$.next(data);
  }

  /**
   * Событие марок
   * @param data передаваемое значение
   */
  public EMark(data?: any) {
    this.mark$.next(data);
  }

  /**
   * Событие доната
   * @param data передаваемое значение
   */
  public EDonat(data?: any) {
    this.donat$.next(data);
  }

  /**
   * Событие предупреждения
   * @param data передаваемое значение
   */
  public EWarn(data?: any) {
    this.warn$.next(data);
  }

  /**
   * Связь со второстепенными компонентами
   */
  public EOther(data?: any) {
    this.other$.next(data);
  }
}
