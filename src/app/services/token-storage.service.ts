import {Injectable} from '@angular/core';
import {CookieService} from "ngx-cookie-service";

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor(private cookieService: CookieService) {
  }

  public saveToken(token: string): void {
    // window.sessionStorage.removeItem(TOKEN_KEY);
    // window.sessionStorage.setItem(TOKEN_KEY, token);
    this.setCookie(TOKEN_KEY, token, 300);
  }

  public getToken(): string {
    return this.getCookie(TOKEN_KEY);
  }

  public saveUser(user: any): void {
    // window.sessionStorage.removeItem(USER_KEY);
    // window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
    user.name = "NoName";
    this.setCookie(USER_KEY, JSON.stringify(user), 300);
  }

  public getUser(): any {
    if(this.getCookie(USER_KEY) == '' || this.getCookie(USER_KEY) == null || this.getCookie(USER_KEY).length <= 0) return null;
    else return JSON.parse(this.getCookie(USER_KEY));
  }

  logOut(): void {
    this.cookieService.delete(USER_KEY);
    this.cookieService.delete(TOKEN_KEY);

    this.setCookie(USER_KEY, "", 700);
    this.setCookie(TOKEN_KEY, "", 700);
  }

  private getCookie(name: string) {

    return this.cookieService.get(name);
  }

  private setCookie(name: string, value: string, expireDays: number, path: string = '') {

    this.cookieService.set(name, value, expireDays);
  }
}
