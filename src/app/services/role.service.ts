import { Injectable } from '@angular/core';
import {UserService} from "./user/user.service";
import {ERole} from "../models/enum/ERole";
import {TokenStorageService} from "./token-storage.service";

/**
 * Данный класс отвечает за взаимодействие с
 * ролью текущего пользователя, а так же с ролями как таковыми
 */
@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private userService: UserService) { }

  /**
   * Возвращает истину если пользователь имеет роль 3 Модератора или больше
   * Ложь в обратом случае
   */
  isModeratorOf_3_LevelAndHigher(): boolean {

    if(this.userService.currentUser == undefined) return false;

    if(this.userService.currentUser.role == ERole.ROLE_MODERATOR_3
      || this.userService.currentUser.role < ERole.ROLE_MODERATOR_3) return true;

    return false;
  }

  /**
   * Возвращает истину если пользователь имеет роль 2 Модератора или больше
   * Ложь в обратом случае
   */
  isModeratorOf_2_LevelAndHigher(): boolean {

    if(this.userService.currentUser == undefined) return false;

    if(this.userService.currentUser.role == ERole.ROLE_MODERATOR_2
    || this.userService.currentUser.role < ERole.ROLE_MODERATOR_2) return true;

    return false;
  }

  /**
   * Админ ли текущий пользователь
   */
  isAdmin(): boolean {
    if(this.userService.currentUser == undefined) return false;
    return this.userService.currentUser.role == ERole.ROLE_ADMIN;
  }

  /**
   * Метод возвращает Истину если пользователь авторизован
   * Ложь в обратном случае
   */
  isUserAuth(): boolean {
    return this.userService.currentUser == undefined? false: true;
  }
}
