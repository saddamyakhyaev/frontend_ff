import { Injectable } from '@angular/core';
import {EventSeason} from "../../models/event/EventSeason";
import {Mark} from "../../models/mark/Mark";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {EnvironmentService} from "../environment.service";

@Injectable({
  providedIn: 'root'
})
export class MarkService {

  URL_API: string;

  /**
   * Все марки
   */
  allMarks: Mark[] = [];

  constructor(private http: HttpClient,
              private environmentService:EnvironmentService) {
    this.URL_API = environmentService.getValue("apiUrl") + '/api/mark/';
  }

  /**
   * Создаём марку
   * @param data - объект для создания
   */
  createMark(data: any): Observable<any> {
    return this.http.post(this.URL_API + "create", data);
  }

  /**
   * Получаем все марки
   */
  getAllMarks(): Observable<any> {
    return this.http.get(this.URL_API + 'all');
  }
}
