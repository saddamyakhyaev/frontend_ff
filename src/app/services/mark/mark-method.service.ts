import { Injectable } from '@angular/core';
import {ClipType} from "../../models/clip/ClipType";
import {MarkService} from "./mark.service";
import {Mark} from "../../models/mark/Mark";

@Injectable({
  providedIn: 'root'
})
export class MarkMethodService {

  constructor(private markService:MarkService) { }

  /**
   * Возвращаем марку по ID
   */
  getMarkById(id: number): Mark {
    for(var i = 0; i < this.markService.allMarks.length; i++){
      if(this.markService.allMarks[i].id == id) return this.markService.allMarks[i];
    }
    return {name: ""};
  }
}
