import { Injectable } from '@angular/core';
import {News} from "../../models/news/News";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Ads} from "../../models/ads/Ads";
import {EnvironmentService} from "../environment.service";

/**
 * Сервис для работы с рекламой
 */
@Injectable({
  providedIn: 'root'
})
export class AdsService {

  URL_API: string;

  /**
   * Загруженные новости
   */
  allAds: Ads[] = [];

  constructor(private http: HttpClient,
              private environmentService:EnvironmentService) {
    this.URL_API = environmentService.getValue("apiUrl") + '/api/ads/';
  }

  /**
   * Создаём рекламу
   * @param data - объект для создания
   */
  createAds(data: any): Observable<any> {
    return this.http.post(this.URL_API + "create", data);
  }

  /**
   * Получаем всю рекламу
   */
  getAllActiveAds(): Observable<any> {
    return this.http.get(this.URL_API + 'all');
  }

  /**
   * Получаем тело рекламы
   * @param adsId id рекламы
   */
  getAdsBody(adsId: number): Observable<any> {
    return this.http.get(this.URL_API + 'body/' + adsId);
  }

  /**
   * Восстанавливаем новость
   * @param adsId id рекламы
   */
  activeAds(adsId: number): Observable<any> {
    return this.http.get(this.URL_API + 'is-active/' + adsId);
  }

  /**
   * Останавливаем новость
   * @param adsId id рекламы
   */
  activeNotAds(adsId: number): Observable<any> {
    return this.http.get(this.URL_API + 'is-not-active/' + adsId);
  }
}
