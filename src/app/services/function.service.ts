import { Injectable } from '@angular/core';
import {EventTime} from "../models/event/EventTime";
import {EventTimeBody} from "../models/event/EventTimeBody";
import {Title} from "@angular/platform-browser";
import {EnvironmentService} from "./environment.service";
import {MessageService} from "primeng/api";

@Injectable({
  providedIn: 'root'
})
export class FunctionService {


  /**
   * Показывать все элементы в списке когда еще будет событие
   */
  isAllTimeAfter: boolean = false;

  constructor(private titleService: Title,
              private messageService: MessageService,
              private environmentService:EnvironmentService ) { }

  /**
   * Метод для трансформации времени для
   * отправки на сервер
   * @param time время для трансформации
   */
  getTimeFormatToSendToServer(time: Date): string {
    return time.toLocaleTimeString(navigator.language, {
      hour: '2-digit',
      minute: '2-digit'
    })
  }


  /**
   * Установить TITLE
   */
  setTitle(str: string): void {
    this.titleService.setTitle(this.environmentService.getValue("name") + " " + str)
  }

  /**
   * Настройка даты
   */
  getDateSetting(date: Date | null): Date {
    if(date == null) return new Date();

    date.setHours(12,12,12)
    return date;
  }

  /**
   * Преобразуем текст {@link EventTime} в дату
   */
  getEventTimeDate(time: EventTime[], dateAfter?: Date | string, eventTime?: EventTime): EventTimeBody[] {

    let dates: EventTimeBody[] = [];

    time.forEach(time_iter => {
      if(time_iter.dates) {
        time_iter.dates.forEach(x => {
            let flag: boolean = true;
            if(dateAfter) {
              if(new Date(dateAfter) > new Date(x)) flag = false;
              if(new Date(dateAfter).toDateString() === new Date(x).toDateString()) {
                if(eventTime) {
                  console.log(eventTime)
                  console.log(time_iter)
                  if (time_iter.timeFrom == eventTime.timeFrom && time_iter.timeTo == eventTime.timeTo) {
                    flag = false;
                  }
                }
              }
            }

            if(flag) dates.push({date: new Date(x) as Date, timeFrom: time_iter.timeFrom, timeTo: time_iter.timeTo})
        });
      }
    })

    dates.sort((x,y) => { return x.date >= y.date ? 1: -1})


    return dates;

  }

  /**
   * Получаем рандомное число
   */
  getRandomInRange(min:number, max:number) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }


  /**
   * Выводим строчную информацию по дате
   */
  getStringDate(date: any): string {

    if(date != null) {
      let nowDate: Date = new Date();
      let lastDate = new Date(date);
      nowDate = new Date(nowDate.valueOf() - (2*60*60*1000));
      if(nowDate < lastDate) return `${lastDate.getHours()}:${lastDate.getMinutes()}`;

      nowDate = new Date(nowDate.valueOf() - (3*60*60*1000));
      if(nowDate < lastDate) return "3 час назад";

      nowDate = new Date(nowDate.valueOf() - (20*60*60*1000));
      if(nowDate < lastDate) return "вчера";

      nowDate = new Date(nowDate.valueOf() - (24*60*60*1000));
      if(nowDate < lastDate) return "позавчера";

      return this.formatDate(lastDate);
    }

    return "-";
  }

  /**
   * Вывод даты
   */
  formatDate(date: any): string {

    var dd:any = date.getDate();
    if (dd < 10) dd = '0' + dd;

    var mm:any = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;

    var yy:any = date.getFullYear() % 100;
    if (yy < 10) yy = '0' + yy;

    return dd + '.' + mm + '.' + yy;
  }

  /**
   * Копировать в буфер обмена
   */
  copyMessageInClipBoard(inputElement: any, succText: string){
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    this.messageService.add({severity: 'success', detail: succText});
  }
}
