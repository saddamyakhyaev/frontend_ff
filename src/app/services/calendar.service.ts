import { Injectable } from '@angular/core';
import {EventTime} from "../models/event/EventTime";


/**
 * Класс для работы с датами
 * По большей части русификация
 */
@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  weeks: string[] = ["Воскресенье", "Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"]
  weeksShort: string[] = ["Вс", "Пн","Вт","Ср","Чт","Пт","Сб"]

  month: string[] = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]
  monthYa: string[] = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"]

  constructor() { }


  /**
   * Возвращем дату в нужном формате
   * @param date
   */
  getDayFormat(date: Date | string): string {

    let dt: Date;

    if(date instanceof Date) dt = date;
    else dt = new Date(date);

    return dt.getDate() + dt.getMonth() + dt.getFullYear() + "";
  }

}
