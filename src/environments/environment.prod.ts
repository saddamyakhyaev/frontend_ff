import {EnvironmentInterface} from "../app/models-client/EnvironmentInterface";

export const environment: EnvironmentInterface = {

  apiUrl: "http://findoffind.xyz:1054",
  production: true,
  name: "Разработка",
  recaptcha_siteKey: "6LceAM8dAAAAABqYKr5zpjolDaB0ebnNyYA8yghX",
  theme_default: 'colorBlue',
  contacts: [
    {name: "Telegram", link: "https://modernrock.ru/posts/blog/bollywoodfm-facts.html"},
    {name: "WhatsApp", link: "https://modernrock.ru/posts/blog/bollywoodfm-facts.html"}
  ]
};
