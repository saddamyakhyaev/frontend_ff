// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {EnvironmentInterface} from "../../app/models-client/EnvironmentInterface";

export const environment: EnvironmentInterface = {

  production: true,

  name: "Найден В РУДН",
  apiUrl: "https://найденврудн.рф",
  recaptcha_siteKey: '6LceAM8dAAAAABqYKr5zpjolDaB0ebnNyYA8yghX',
  theme_default: 'colorWhiteBlue',
  contacts: []
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
